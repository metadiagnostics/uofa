/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test;

import com.cognitivemedicine.sample.test.util.MockKTDFhirBaseService;
import com.cognitivemedicine.sample.test.util.MockReasoningRestClient;
import com.cognitivemedicine.sample.test.util.MockSimulatorRestClient;
import com.cognitivemedicine.sample.test.util.RuleTestUtils;
import com.cognitivemedicine.sample.test.util.nec.NECTestExecutor;
import com.cognitivemedicine.sample.test.util.nec.NECTestModel;
import com.cognitivemedicine.sample.test.util.nec.NECTestParser;
import org.junit.Before;
import org.junit.Test;
import org.kie.api.KieBase;

import java.util.List;


public class CaregiverComplianceProcessAdherenceTest {

    private KieBase kbase;

    private MockKTDFhirBaseService fhirService;
    private MockReasoningRestClient outcomeService;
    private MockSimulatorRestClient simulatorService;

    @Before
    public void doBefore() {
        
        fhirService = new MockKTDFhirBaseService();
        outcomeService = new MockReasoningRestClient();
        simulatorService = new MockSimulatorRestClient();
        
        kbase = RuleTestUtils.getKieBase("uofASampleDefaultKB");

    }

    @Test
    public void doLessThan7DaysNoFormPATest() {

        String header = "id,birthDate,birthWeight,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-09 08:00:00,"
            + "900,"
            + "2018-02-13 08:00:00,"
            + "Risk Scoring|fail,"
            + "GutCheckNEC Scoring|Non-Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
        
    }
    
    @Test
    public void doLessThan7DaysFormPATest() {
        String header = "id,birthDate,birthWeight,formResponse,now,result,resultCriteria";
        String data = "1,"
            + "2018-02-09 08:00:00,"
            + "900,"
            + "1|2018-02-12 08:00:00,"
            + "2018-02-13 08:00:00,"
            + "Risk Scoring|ok,"
            + "GutCheckNEC Scoring|Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    
    @Test
    public void doMoreThan7DaysMoreThan2FormsOnFirst7DaysNoFormAfterPATest() {
        String header = "id,birthDate,birthWeight,formResponse,formResponse,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-12 09:10:00,"
            + "900,"
            + "1|2018-02-12 09:20:00,"                            //Before 7th day
            + "2|2018-02-13 09:20:00,"                            //Before 7th day
            + "2018-02-22 09:10:00,"
            + "Risk Scoring|caution,"
            + "GutCheckNEC Scoring|Partially Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doMoreThan7DaysMoreThan2FormsOnFirst7DaysFormAfterPATest() {
        String header = "id,birthDate,birthWeight,formResponse,formResponse,formResponse,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-12 09:10:00,"
            + "900,"
            + "1|2018-02-12 09:20:00,"                            //Before 7th day
            + "2|2018-02-13 09:20:00,"                            //Before 7th day
            + "3|2018-02-20 09:20:00,"                            //After 7th day
            + "2018-02-22 09:10:00,"
            + "Risk Scoring|ok,"
            + "GutCheckNEC Scoring|Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doMoreThan7Days1FormOnFirst7DaysNoFormsAfterPATest() {
        String header = "id,birthDate,birthWeight,formResponse,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-12 09:10:00,"
            + "900,"
            + "1|2018-02-12 09:20:00,"                            //Before 7th day
            + "2018-02-22 09:10:00,"
            + "Risk Scoring|caution,"
            + "GutCheckNEC Scoring|Partially Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doMoreThan7Days1FormOnFirst7Days1FormAfterPATest() {
        String header = "id,birthDate,birthWeight,formResponse,formResponse,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-12 09:10:00,"
            + "900,"
            + "1|2018-02-12 09:20:00,"                            //Before 7th day
            + "2|2018-02-20 09:20:00,"                            //After 7th day
            + "2018-02-22 09:10:00,"
            + "Risk Scoring|ok,"
            + "GutCheckNEC Scoring|Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doMoreThan7DaysNoFormsOnFirst7Days1OrLessFormAfterPATest_1() {
        String header = "id,birthDate,birthWeight,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-12 09:10:00,"
            + "900,"
            + "2018-02-22 09:10:00,"
            + "Risk Scoring|fail,"
            + "GutCheckNEC Scoring|Non-Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doMoreThan7DaysNoFormsOnFirst7Days1OrLessFormAfterPATest_2() {
        String header = "id,birthDate,birthWeight,formResponse,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-12 09:10:00,"
            + "900,"
            + "1|2018-02-20 09:20:00,"                            //After 7th day
            + "2018-02-22 09:10:00,"
            + "Risk Scoring|fail,"
            + "GutCheckNEC Scoring|Non-Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doMoreThan7DaysNoFormsOnFirst7Days2OrMoreFormAfterPATest() {
        String header = "id,birthDate,birthWeight,formResponse,formResponse,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-12 09:10:00,"
            + "900,"
            + "1|2018-02-20 09:20:00,"                            //After 7th day
            + "2|2018-02-21 09:20:00,"                            //After 7th day
            + "2018-02-22 09:10:00,"
            + "Risk Scoring|caution,"
            + "GutCheckNEC Scoring|Partially Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
}
