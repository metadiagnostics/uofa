/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.util;

import ca.uhn.fhir.rest.client.IGenericClient;
import com.cognitivemedicine.sample.fhir.KTDFhirBaseService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import static java.util.stream.Collectors.toList;

import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;

/**
 *
 * @author esteban
 */
public class MockKTDFhirBaseService extends KTDFhirBaseService {

    private Map<String, IBaseResource> resources = new HashMap<>();

    public MockKTDFhirBaseService() {
        super((IGenericClient)null);
    }

    @Override
    public List<Patient> listActivePatients() {
        return this.searchResourceByClass(Patient.class).stream()
            .filter(p -> p.getActive())
            .collect(toList());
    }

    @Override
    public List<MedicationAdministration> listMedicationAdministrations(String patientId, String codeSystem, String code) {
        return this.resources.values().stream()
            .filter(r -> r instanceof MedicationAdministration)
            .map(r -> (MedicationAdministration) r)
            .filter(m -> {
                try {
                    return patientId.equals(((Patient)m.getSubject().getResource()).getId()) && m.getMedicationCodeableConcept().getCodingFirstRep().getCode().equals(code);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            })
            .collect(toList());
    }

    @Override
    public List<Observation> listObservations(String patientId, String codeSystem, String code) {
        return this.resources.values().stream()
                .filter(r -> r instanceof Observation)
                .map(r -> (Observation) r)
                .filter(o -> {
                    try {
                        return patientId.equals(((Patient)o.getSubject().getResource()).getId()) && o.hasCode() && o.getCode().getCodingFirstRep().getCode().equals(code);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return false;
                })
                .collect(toList());
    }

    @Override
    public List<DiagnosticReport> listDiagnosticReports(String patientId) {
        return this.resources.values().stream()
                .filter(r -> r instanceof DiagnosticReport)
                .map(r -> (DiagnosticReport) r)
                .filter(o -> {
                    try {
                        return patientId.equals(((Patient)o.getSubject().getResource()).getId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return false;
                })
                .collect(toList());
    }
    
    @Override
    public List<NutritionOrder> listNutritionOrders(String patientId) {
        return this.resources.values().stream()
                .filter(r -> r instanceof NutritionOrder)
                .map(r -> (NutritionOrder) r)
                .filter(o -> {
                    try {
                        return patientId.equals(((Patient)o.getPatient().getResource()).getId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return false;
                })
                .collect(toList());
    }

    @Override
    public void insertMedicationAntibioticObservation(Patient patient, double value) {
        Observation obs = this.buildMedicationAntibioticObservation(patient, value);
        this.createOrUpdateResource(obs);
    }
    
    @Override
    public void createOrUpdateTrophicFeedingObservation(Patient patient, double value) {
        List<Observation> observations = this.listObservations(patient.getIdElement().getIdPart(), "http://cognitivemedicine.info", "X-trophic-feeding-history");
        
        if (observations.size() > 1){
            throw new IllegalStateException("More than 1 Trophic Feeding Observations found for patient "+patient.getId());
        }
        
        Observation obs;
        if (observations.isEmpty()){
            //Create a new Observation
            obs = this.buildTrophicFeedingObservation(patient, value);
        } else {
            //Update the existing Observation
            obs = observations.get(0);
            obs.setValue(new SimpleQuantity().setValue(value));
        }
        
        //Persist
        this.createOrUpdateResource(obs);
    }

    @Override
    public <T extends IBaseResource> T createOrUpdateResource(T resource) {
        String id = resource.getIdElement() != null && resource.getIdElement().getIdPart() != null ? resource.getIdElement().getIdPart()
                : UUID.randomUUID().toString();
        IBaseResource r = resources.get(id);
        if (r == null) {
            resource.setId(id);
        }
        this.resources.put(id, resource);

        return resource;
    }
    
    public Map<String, IBaseResource> getResources() {
        return resources;
    }

    public <T extends IBaseResource> List<T> searchResourceByIdentifier(String codeSystem, String code, Class<T> clazz) {

        List<T> results = new ArrayList<>();
        if (!Resource.class.isAssignableFrom(clazz)) {
            return results;
        }

        for (IBaseResource r : resources.values()) {
            if (clazz.isAssignableFrom(r.getClass())) {
                try {
                    Base[] identifiers = ((Resource) r).getProperty("identifier".hashCode(), "identifier", true);
                    if (identifiers == null) {
                        continue;
                    }

                    String c = Arrays.stream(identifiers).filter(id -> id instanceof Identifier).map(id -> (Identifier) id)
                            .filter(id -> codeSystem.equals(id.getSystem())).findFirst()
                            .orElse(new Identifier().setSystem(codeSystem).setValue(null)).getValue();

                    if (code.equals(c)) {
                        results.add((T) r);
                    }
                } catch (FHIRException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        }

        return results;
    }

    public <T extends IBaseResource> T searchResourceById(IIdType idType, Class<T> clazz) {
        String id = idType.getIdPart();

        IBaseResource resource = resources.get(id);

        if (resource == null) {
            return null;
        }

        if (!clazz.isAssignableFrom(resource.getClass())) {
            throw new IllegalArgumentException("The resource with id '" + id + "' is not of type '" + clazz.getName() + "' but of type '"
                    + resource.getClass().getName() + "'");
        }

        return (T) resource;
    }

    public <T extends IBaseResource> List<T> searchResourceByClass(Class<T> clazz) {
        return resources.values().stream().filter(r -> clazz.isAssignableFrom(r.getClass())).map(r -> (T) r).collect(toList());
    }
}
