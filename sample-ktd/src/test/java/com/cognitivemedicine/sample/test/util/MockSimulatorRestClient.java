/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.util;

import com.cognitivemedicine.cs.client.DataServiceRestClient;
import com.cognitivemedicine.cs.models.FormResponse;
import com.cognitivemedicine.cs.models.FormResponseItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MockSimulatorRestClient extends DataServiceRestClient {

    private Map<String, List<FormResponse>> formResponsesByPatient = new HashMap<>();

    @Override
    public String addFormResponse(String json) {
        FormResponse response = null;
        try {
            response = RuleTestUtils.serializeFormResponse(json);
        } catch (IOException io) {
            io.printStackTrace();
        }

        if (response != null) {
            List<FormResponse> existingResponses = this.formResponsesByPatient.get(response.getPatientId());

            if (existingResponses == null) {
                existingResponses = new ArrayList<>();
                this.formResponsesByPatient.put(response.getPatientId(), existingResponses);
            }

            existingResponses.add(response);
            return response.getId();
        }

        return null;
    }

    @Override
    public FormResponse[] retrieveFormResponses(String patientId, String formType, String formVersion) {
        List<FormResponse> responses = this.formResponsesByPatient.get(patientId);
        return responses == null ? new FormResponse[0] : responses.toArray(new FormResponse[responses.size()]);
    }
}
