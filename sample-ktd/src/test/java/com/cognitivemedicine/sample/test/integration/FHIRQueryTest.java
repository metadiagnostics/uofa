/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.integration;

import com.cognitivemedicine.sample.fhir.DiagnosticReportResult;
import com.cognitivemedicine.sample.fhir.FhirBaseConfigurator;
import com.cognitivemedicine.sample.fhir.KTDFhirBaseService;
import org.hl7.fhir.dstu3.model.DiagnosticReport;
import org.hl7.fhir.dstu3.model.MedicationAdministration;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.ProcedureRequest;
import org.hl7.fhir.exceptions.FHIRException;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/*
 This class is purely used for integration testing FHIR queries and shouldn't actually be committed
 as running unit tests
 */
public class FHIRQueryTest {
    public void testBreastMilkMARetrieval() throws FHIRException {
        KTDFhirBaseService service = FhirBaseConfigurator.newInstance();

        List<MedicationAdministration> meds = service.listMedicationAdministrations("PatientId-1234", "http://snomed.info/sct", "226789007");
        assertThat(meds.size(), not(0));
        assertThat(meds.get(0).getMedicationCodeableConcept().getCodingFirstRep().getCode(), is("226789007"));
        assertThat(meds.get(0).getDosage().getDose().getValue().toBigInteger().intValue(), is(25));
    }

    public void testFormulaMARetrieval() throws FHIRException {
        KTDFhirBaseService service = FhirBaseConfigurator.newInstance();

        List<MedicationAdministration> meds = service.listMedicationAdministrations("PatientId-1234", "http://snomed.info/sct", "386127005");
        assertThat(meds.size(), not(0));
        assertThat(meds.get(0).getMedicationCodeableConcept().getCodingFirstRep().getCode(), is("386127005"));
        assertThat(meds.get(0).getDosage().getDose().getValue().toBigInteger().intValue(), is(25));
    }

    public void testBirthWeightRetrieval() throws FHIRException {
        KTDFhirBaseService service = FhirBaseConfigurator.newInstance();

        List<Observation> obs = service.listObservations("PatientId-1234", "http://snomed.info/sct", "364589006");
        assertThat(obs.size(), not(0));
        assertThat(obs.get(0).getCode().getCodingFirstRep().getCode(), is("364589006"));
        assertThat(obs.get(0).getValueQuantity().getValue().toBigInteger().intValue(), is(950));
    }

    public void testDailyWeightRetrieval() throws FHIRException {
        KTDFhirBaseService service = FhirBaseConfigurator.newInstance();

        List<Observation> obs = service.listObservations("PatientId-1234", "http://snomed.info/sct", "27113001");
        assertThat(obs.size(), not(0));
        assertThat(obs.get(0).getCode().getCodingFirstRep().getCode(), is("27113001"));
        assertThat(obs.get(0).getValueQuantity().getValue().toBigInteger().intValue(), is(920));
    }

    public void testUrineCulture() {
        KTDFhirBaseService service = FhirBaseConfigurator.newInstance();

        List<DiagnosticReport> reports = service.listDiagnosticReports("PatientId-1234");
        assertThat(reports.size(), not(0));
    }
}
