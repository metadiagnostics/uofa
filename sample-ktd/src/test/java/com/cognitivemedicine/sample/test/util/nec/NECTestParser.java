/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.util.nec;

import com.cognitivemedicine.cs.models.FormResponse;
import com.cognitivemedicine.sample.fhir.DiagnosticReportResult;
import com.cognitivemedicine.sample.model.Now;
import com.cognitivemedicine.sample.test.util.FhirTestUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;

/**
 *
 * @author esteban
 */
public class NECTestParser {

    public static final String COL_ID = "id";
    public static final String COL_PATIENT_ID = "patientId";  //If present, the value will be used as the patient Id, if not a patient with a random UUID will be created.
    public static final String COL_NOW = "now";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_BIRTH_DATE = "birthDate";
    public static final String COL_BIRTH_WEIGHT = "birthWeight";
    public static final String COL_FEEDING = "feeding";
    public static final String COL_MEDICATION = "medication";
    public static final String COL_DAILY_WEIGHT = "dailyWeight";
    public static final String COL_NUTRITION_ORDER = "nutritionOrder";
    public static final String COL_DIAGNOSTIC_REPORT = "diagnosticReport";
    public static final String COL_FORM_RESPONSE = "formResponse";
    public static final String COL_RESULT = "result";
    public static final String COL_RESULT_CRITERIA = "resultCriteria";
    public static final String COL_OBSERVATION = "observation";
    public static final String COL_CONDITION = "condition";

    public static final String FIELD_SEPARATOR = ",";
    public static final String VALUE_SEPARATOR = "\\|";

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private final SimpleDateFormat formResponseDateFormat = new SimpleDateFormat("yyyyMMddHHmm");


    public List<NECTestModel> parse(File csv) throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader(csv));

        String header = null;
        List<String> data = new ArrayList<>();

        String line;
        while ((line = reader.readLine()) != null) {
            if (header == null) {
                header = line;
            } else {
                data.add(line);
            }
        }

        return this.parse(header, data);
    }

    public List<NECTestModel> parse(String header, String data) {
        String[] dataLines = data.split("[\\r\\n]+");
        return this.parse(header, Arrays.asList(dataLines));
    }
    
    public List<NECTestModel> parse(String header, List<String> data) {

        List<NECTestModel> result = new ArrayList<>();

        String[] colNames = header.split(FIELD_SEPARATOR);

        //Let's locate first the birthDate. We need it to create the patient
        //and all the other resources
        int birthDateIndex = -1;
        for (int i = 0; i < colNames.length; i++) {
            if (COL_BIRTH_DATE.equals(colNames[i])) {
                birthDateIndex = i;
                break;
            }
        }
        if (birthDateIndex == -1) {
            throw new IllegalArgumentException("The birth date of the Patient was not provided");
        }

        //Parse every line
        for (String line : data) {
            NECTestModel model = new NECTestModel();
            String[] dataCells = line.split(FIELD_SEPARATOR);
            
            //We need to get the patientId first if present
            String patientId = random();
            for (int i = 0; i < colNames.length; i++) {
                if (COL_PATIENT_ID.equals(colNames[i]) && !dataCells[i].trim().isEmpty()){
                    patientId = dataCells[i].trim();
                    model.setPredefinedPatientId(patientId);
                    break;
                }
            }
            
            //Patient Creation
            Patient patient = FhirTestUtil.buildPatient(patientId);
            model.setPatient(patient);
            
            NECTestModel.NECTestResult lastTestResult = null;
            for (int i = 0; i < colNames.length; i++) {

                if (colNames.length != dataCells.length) {
                    throw new IllegalStateException("The header and data count doesn't match: " + colNames.length + " != " + dataCells.length);
                }

                String dc = dataCells[i].trim();
                
                if (dc.isEmpty()){
                    continue;
                }
                
                switch (colNames[i]) {
                    case COL_ID:
                        model.setId(dc);
                        break;
                    case COL_NOW:
                        model.setNow(parseNow(dc));
                        break;
                    case COL_PATIENT_ID:
                        //do nothing
                        break;
                    case COL_DESCRIPTION:
                        model.setDescription(dc);
                        break;
                    case COL_BIRTH_DATE:
                        patient.setBirthDate(parseBirthDate(dc));
                        break;
                    case COL_BIRTH_WEIGHT:
                        model.setBirthWeight(parseBirthWeight(patient, dc));
                        break;
                    case COL_FEEDING:
                    case COL_MEDICATION:
                        model.addFeeding(parseFeeding(patient, dc));
                        break;
                    case COL_DAILY_WEIGHT:
                        model.addDailyWeight(parseDailyWeight(patient, dc));
                        break;
                    case COL_NUTRITION_ORDER:
                        model.addNutritionOrder(parseNutritionOrder(patient, dc));
                        break;
                    case COL_DIAGNOSTIC_REPORT:
                        model.addDiagnosticReport(parseDiagnosticReport(model, patient, dc));
                        break;
                    case COL_OBSERVATION:
                        model.addObservation(parseObservation(patient, dc));
                        break;
                    case COL_FORM_RESPONSE:
                        model.addFormResponse(parseFormResponse(patient, dc));
                        break;
                    case COL_RESULT:
                        lastTestResult = parseResult(dc);
                        model.addResult(lastTestResult);
                        break;
                    case COL_RESULT_CRITERIA:
                        if (lastTestResult == null){
                            throw new IllegalStateException("Column '"+COL_RESULT_CRITERIA+"' must be defined AFTER column '"+COL_RESULT+"'");
                        }
                        lastTestResult.addCriteria(parseResultCriteria(dc));
                        break;
                    case COL_CONDITION:
                        model.addCondition(parseCondition(patient, dc));
                        break;
                }

            }
            result.add(model);
        }

        return result;
    }

    private Now parseNow(String data) {
        return new Now(toDate(COL_NOW, data));
    }

    private Date parseBirthDate(String data) {
        return toDate(COL_BIRTH_DATE, data);
    }

    private Observation parseBirthWeight(Patient patient, String data) {
        try {
            return FhirTestUtil.buildObservation(patient, "bw-" + random(), "364589006", null, "Birth weight", Long.parseLong(data));
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException(COL_BIRTH_WEIGHT + " can't be parsed", ex);
        }
    }

    private MedicationAdministration parseFeeding(Patient patient, String data) {

        String[] vals = data.split(VALUE_SEPARATOR);
        if (vals.length != 2 && vals.length != 3) {
            throw new IllegalArgumentException("Unsupported format for " + COL_FEEDING + ". Supported values are: 'code" + VALUE_SEPARATOR + "dose' or 'code" + VALUE_SEPARATOR + "dose" + VALUE_SEPARATOR + "timestamp'");
        }

        String code = vals[0];
        Long dose;
        try {
            dose = Long.parseLong(vals[1]);
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Unable to parse " + COL_FEEDING + "'s dose value as a Long ", ex);
        }

        Date timestamp = null;
        if (vals.length >= 3) {
            timestamp = toDate(COL_FEEDING, vals[2]);
        }

        MedicationAdministration feeding = FhirTestUtil.buildMedication(patient, "ma-" + random(), code, "", dose);
        if (timestamp != null) {
            feeding.setEffective(new DateTimeType(timestamp));
        }

        return feeding;
    }

    private Observation parseDailyWeight(Patient patient, String data) {

        String[] vals = data.split(VALUE_SEPARATOR);
        if (vals.length != 2) {
            throw new IllegalArgumentException("Unsupported format for " + COL_DAILY_WEIGHT + ". Supported values is: 'value" + VALUE_SEPARATOR + "timestamp'");
        }

        Long value;
        try {
            value = Long.parseLong(vals[0]);
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Unable to parse " + COL_DAILY_WEIGHT + "'s value as a Long ", ex);
        }

        Date timestamp = toDate(COL_DAILY_WEIGHT, vals[1]);

        Observation dailyWeight = FhirTestUtil.buildObservation(patient, "dw-" + random(), "27113001", null, "Daily weight", value);
        if (timestamp != null) {
            dailyWeight.setEffective(new DateTimeType(timestamp));
        }

        return dailyWeight;
    }

    private NutritionOrder parseNutritionOrder(Patient patient, String data) {

        String[] vals = data.split(VALUE_SEPARATOR);
        if (vals.length != 1 && vals.length != 2) {
            throw new IllegalArgumentException("Unsupported format for " + COL_NUTRITION_ORDER + ". Supported values are: 'status' or 'status" + VALUE_SEPARATOR + "timestamp'");
        }

        NutritionOrder.NutritionOrderStatus status;
        try {
            status = NutritionOrder.NutritionOrderStatus.fromCode(vals[0].toLowerCase());
        } catch (FHIRException ex) {
            throw new IllegalArgumentException(COL_NUTRITION_ORDER + " value can't be converted to a NutritionOrder.NutritionOrderStatus", ex);
        }

        Date timestamp = null;
        if (vals.length >= 2) {
            timestamp = toDate(COL_NUTRITION_ORDER, vals[1]);
        }

        NutritionOrder order = FhirTestUtil.buildNutritionOrder(patient, "no-" + random(), "http://cognitivemedicine.com/vs", "X-feeding-order-1", status, timestamp);

        return order;
    }

    private DiagnosticReport parseDiagnosticReport(NECTestModel model, Patient patient, String data) {
        
        String[] vals = data.split(VALUE_SEPARATOR);
        if (vals.length != 2) {
            throw new IllegalArgumentException("Unsupported format for " + COL_DIAGNOSTIC_REPORT + ". Supported values are: 'status' or 'code" + VALUE_SEPARATOR + "resultCode");
        }
        
        String code = vals[0];
        String resultCode = vals[1];

        ProcedureRequest procedureRequest = FhirTestUtil.buildProcedureRequest(patient, "pr-" + random(), code, "http://snomed.info/sct");
        model.addProcedureRequest(procedureRequest);

        Observation observation = FhirTestUtil.buildObservation(patient, "obs-" + random(), resultCode, "http://hl7.org/fhir/v2/0078");
        model.addObservation(observation);

        return FhirTestUtil.buildDiagnosticReport(patient, procedureRequest, observation);
    }

    private Observation parseObservation(Patient patient, String data) {
        String[] vals = data.split(VALUE_SEPARATOR);
        if (vals.length != 4) {
            throw new IllegalArgumentException("Unsupported format for " + COL_OBSERVATION + ". Supported values are: 'code'" + VALUE_SEPARATOR + "codeSystem"+ VALUE_SEPARATOR +"display" + VALUE_SEPARATOR + "'value'");
        }

        String code = vals[0];
        String codeSystem = vals[1];
        String display = vals[2];
        double value = Double.parseDouble(vals[3]);

        return FhirTestUtil.buildObservation(patient, "ob-" + random(),  code, codeSystem, display, value);
    }

    private Condition parseCondition(Patient patient, String data) {
        String[] vals = data.split(VALUE_SEPARATOR);
        if (vals.length != 3) {
            throw new IllegalArgumentException("Unsupported format for " + COL_CONDITION + ". Supported values are: 'code'" + VALUE_SEPARATOR + "'display name'" + VALUE_SEPARATOR + "'onset date/time'");
        }

        String code = vals[0];
        String codeSystem = "http://snomed.info/sct";
        String display = vals[1];
        Date onsetDateTime = toDate(COL_OBSERVATION, vals[2]);
        return FhirTestUtil.buildCondition(patient, "co-" + random(), code, codeSystem, display, onsetDateTime);
    }
    
    private FormResponse parseFormResponse(Patient patient, String data) {
        String[] vals = data.split(VALUE_SEPARATOR);
        if (vals.length != 2) {
            throw new IllegalArgumentException("Unsupported format for " + COL_FORM_RESPONSE + ". Supported values are: 'id'" + VALUE_SEPARATOR + "timestamp");
        }

        String id = vals[0];
        Date timestamp = toDate(COL_FORM_RESPONSE, vals[1]);
        String createdDateTime = formResponseDateFormat.format(timestamp);

        return this.buildFormResponse(id, createdDateTime, patient.getIdElement().getIdPart());
    }
    
    private NECTestModel.NECTestResult parseResult(String data) {
        String[] vals = data.split(VALUE_SEPARATOR);
        if (vals.length != 2) {
            throw new IllegalArgumentException("Unsupported format for " + COL_RESULT + ". Supported values is: 'key" + VALUE_SEPARATOR + "status'");
        }
        
        return new NECTestModel.NECTestResult(vals[0], vals[1]);
    }
    
    private NECTestModel.NECTestResultCriteria parseResultCriteria(String data) {
        String[] vals = data.split(VALUE_SEPARATOR);
        if (vals.length != 2) {
            throw new IllegalArgumentException("Unsupported format for " + COL_RESULT_CRITERIA + ". Supported values is: 'name" + VALUE_SEPARATOR + "compliancy'");
        }
        return new NECTestModel.NECTestResultCriteria(vals[0], vals[1]);
    }

    private String random() {
        return UUID.randomUUID().toString();
    }

    private Date toDate(String fieldName, String fieldValue) {
        try {
            return dateFormat.parse(fieldValue);
        } catch (ParseException ex) {
            throw new IllegalArgumentException("Unable to parse field " + fieldName + " to a Date. The value of the field is: " + fieldValue);
        }
    }

    private FormResponse buildFormResponse(String id, String createdDateTime, String patientId) {
        FormResponse response = new FormResponse();
        response.setId(id);
        response.setPatientId(patientId);
        response.setSmartformType("gutchecknec");
        response.setSmartformVersion("1.0");
        response.setCreatedDateTime(createdDateTime);
        response.setResponses(new ArrayList<>());
        return response;
    }
    
                }
