/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.util;

import org.hl7.fhir.dstu3.model.*;

import java.util.*;

public class FhirTestUtil {

    public static MedicationAdministration buildMedication(Patient patient, String id, String code, String display, long doseValue) {
        MedicationAdministration med = new MedicationAdministration();
        med.setId(id);
        CodeableConcept codeableConcept = buildCodeableConcept(code, display);
        med.setMedication(codeableConcept);
        MedicationAdministration.MedicationAdministrationDosageComponent dosage = new MedicationAdministration.MedicationAdministrationDosageComponent();
        SimpleQuantity doseQuantity = new SimpleQuantity();
        doseQuantity.setValue(doseValue);
        dosage.setDose(doseQuantity);
        med.setDosage(dosage);

        Reference ref = new Reference("Patient/" + patient.getIdElement().getIdPart());
        ref.setResource(patient);
        med.setSubject(ref);

        return med;
    }

    public static Observation buildObservation(Patient patient, String id, String code, String codeSystem, String display, double value) {
        Observation obs = new Observation();
        obs.setId(id);
        CodeableConcept codeableConcept = codeSystem == null ? buildCodeableConcept(code, display) : buildCodeableConcept(codeSystem, code, display);
        obs.setCode(codeableConcept);
        Quantity quantity = new SimpleQuantity();
        quantity.setValue(value);
        obs.setValue(quantity);

        Reference ref = new Reference("Patient/" + patient.getIdElement().getIdPart());
        ref.setResource(patient);
        obs.setSubject(ref);

        return obs;
    }

    public static Observation buildObservation(Patient patient, String id, String interpretationCode, String interpretationCodeSystem) {
        Reference ref = new Reference("Patient/" + patient.getIdElement().getIdPart());
        ref.setResource(patient);

        Observation obs = new Observation();
        obs.setId(id);
        obs.setSubject(ref);
        CodeableConcept resultConcept = buildCodeableConcept(interpretationCodeSystem, interpretationCode, "");
        obs.setInterpretation(resultConcept);

        return obs;
    }
    
    public static NutritionOrder buildNutritionOrder(Patient patient, String id, String codeSystem, String code, NutritionOrder.NutritionOrderStatus status, Date timestamp) {
        NutritionOrder order = new NutritionOrder();
        order.setId(id);
        order.setStatus(status);
        order.setDateTime(timestamp);

        Reference ref = new Reference("Patient/" + patient.getIdElement().getIdPart());
        ref.setResource(patient);
        order.setPatient(ref);

        NutritionOrder.NutritionOrderOralDietComponent diet = new NutritionOrder.NutritionOrderOralDietComponent();
        diet.addType(buildCodeableConcept(codeSystem, code, ""));
        order.setOralDiet(diet);
        
        return order;
    }

    public static DiagnosticReport buildDiagnosticReport(Patient patient, ProcedureRequest procedureRequest, Observation resultObservation) {
        DiagnosticReport report = new DiagnosticReport();

        Reference ref = new Reference("Patient/" + patient.getIdElement().getIdPart());
        ref.setResource(patient);

        report.setSubject(ref);

        Reference procReference = new Reference("ProcedureRequest/" + procedureRequest.getId());
        procReference.setResource(procedureRequest);

        List<Reference> basedOnReferenceList = new ArrayList<>();
        basedOnReferenceList.add(procReference);
        report.setBasedOn(basedOnReferenceList);

        List<Reference> resultReferenceList = new ArrayList<>();

        Reference obsRef = new Reference("Observation/" + resultObservation.getId());
        obsRef.setResource(resultObservation);
        resultReferenceList.add(obsRef);

        report.setResult(resultReferenceList);
        return report;
    }

    public static ProcedureRequest buildProcedureRequest(Patient patient, String id, String code, String codeSystem) {
        Reference ref = new Reference("Patient/" + patient.getIdElement().getIdPart());
        ref.setResource(patient);

        ProcedureRequest request = new ProcedureRequest();
        request.setId(id);
        request.setSubject(ref);
        CodeableConcept concept = buildCodeableConcept(codeSystem, code, "");
        request.setCode(concept);
        return request;
    }

    public static Condition buildCondition(Patient patient, String id, String code, String codeSystem, String display, Date onsetDateTime) {
        Reference ref = new Reference("Patient/" + patient.getIdElement().getIdPart());
        ref.setResource(patient);

        Condition condition = new Condition();
        condition.setSubject(ref);
        condition.setId(id);
        condition.setCode(buildCodeableConcept(codeSystem, code, display));
        condition.setOnset(new DateTimeType(onsetDateTime));
        return condition;
    }

    public static CodeableConcept buildCodeableConcept(String code, String display) {
        return buildCodeableConcept("http://snomed.info/sct", code, display);
    }
    
    public static CodeableConcept buildCodeableConcept(String codeSystem, String code, String display) {
        CodeableConcept codeableConcept = new CodeableConcept();
        List<Coding> codings = new ArrayList<>();
        codings.add(new Coding(codeSystem, code, display));
        codeableConcept.setCoding(codings);
        return codeableConcept;
    }

    public static Patient buildPatient(Integer daysOld) {
        return buildPatient(null, daysOld);
    }
    
    public static Patient buildPatient(String id, Integer daysOld) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -daysOld);
        return buildPatient(id, cal.getTime());
    }
    
    public static Patient buildPatient(Date birthDate) {
        return buildPatient(null, birthDate);
    }
    
    public static Patient buildPatient(String id) {
        return buildPatient(id, (Date) null);
    }
    
    public static Patient buildPatient(String id, Date birthDate) {
        Patient patient = new Patient();
        patient.setActive(true);
        patient.setBirthDate(birthDate);
        patient.setId(id);
        
        return patient;
    }
    
}
