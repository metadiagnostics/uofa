/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test;

import com.cognitivemedicine.sample.test.util.*;
import com.cognitivemedicine.sample.test.util.nec.NECTestExecutor;
import com.cognitivemedicine.sample.test.util.nec.NECTestModel;
import com.cognitivemedicine.sample.test.util.nec.NECTestParser;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.exceptions.FHIRException;
import org.junit.Before;
import org.junit.Test;
import org.kie.api.KieBase;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MedicationStewardshipProcessAdherenceTest {

    private KieBase kbase;

    private MockKTDFhirBaseService fhirService;
    private MockReasoningRestClient outcomeService;
    private MockSimulatorRestClient simulatorService;

    @Before
    public void doBefore() {

        fhirService = new MockKTDFhirBaseService();
        outcomeService = new MockReasoningRestClient();
        simulatorService = new MockSimulatorRestClient();

        kbase = RuleTestUtils.getKieBase("uofASampleDefaultKB");
    }

    @Test
    public void doNoH2BlockerPATest() {
        
        String header = "id,birthDate,birthWeight,now,result,resultCriteria,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-11 08:00:00,"
            + "900,"
            + "2018-02-14 08:00:00,"
            + "Medication Stewardship|ok,"
            + "H2 Blocker|Compliant,"
            + "Antibiotic Administration|Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
        
    }

    @Test
    public void doPatientLessThan96HoursOldTest() {
        String header = "id,birthDate,birthWeight,now,result,resultCriteria,resultCriteria";
        String data = ""
                + "1,"
                + "2018-02-11 08:00:00,"
                + "900,"
                + "2018-02-13 08:00:00,"
                + "Medication Stewardship|ok,"
                + "H2 Blocker|Compliant,"
                + "Antibiotic Administration|Compliant";

        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);

    }

    @Test
    public void doMicrobiologyCulturesAndNoAntibioticsTest() throws FHIRException {
        
        String header = "id,birthDate,birthWeight,diagnosticReport,diagnosticReport,medication,now,result,resultCriteria,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-12 09:10:00,"
            + "900,"
            + "117010004|NEG,"          //URINE
            + "252399001|NEG,"          //CSF
            + "372755005|50|2018-02-16 09:10:00,"             //H2
            + "2018-02-18 09:10:00,"
            + "Medication Stewardship|caution,"
            + "H2 Blocker|Non-Compliant,"
            + "Antibiotic Administration|Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
        List<Observation> obs = fhirService.listObservations(testModel.get(0).getPatient().getIdElement().getIdPart(), "http://cognitivemedicine.info", "med-stewardship-antibiotic");
        assertThat(obs.size(), is(1));
        assertThat(obs.get(0).getValueQuantity().getValue().doubleValue(), is(0.5));
    }

    @Test
    public void doMicrobiologyCulturesWithAntibioticsTest() throws FHIRException {
        
        String header = "id,birthDate,birthWeight,diagnosticReport,diagnosticReport,medication,medication,now,result,resultCriteria,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-12 09:10:00,"
            + "900,"
            + "117010004|NEG,"          //URINE
            + "252399001|NEG,"          //SPINAL
            + "372755005|50|2018-02-16 09:10:00,"             //H2
            + "350121009|90|2018-02-16 09:10:00,"             //ANTIBIOTIC
            + "2018-02-18 09:10:00,"
            + "Medication Stewardship|fail,"
            + "H2 Blocker|Non-Compliant,"
            + "Antibiotic Administration|Non-Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
        List<Observation> obs = fhirService.listObservations(testModel.get(0).getPatient().getIdElement().getIdPart(), "http://cognitivemedicine.info", "med-stewardship-antibiotic");
        assertThat(obs.size(), is(1));
        assertThat(obs.get(0).getValueQuantity().getValue().doubleValue(), is(0.0));
    }

    @Test
    public void doMicrobiologyCulturesWithPositiveResultAndNoAntibiotics() throws FHIRException {
        
        String header = "id,birthDate,birthWeight,diagnosticReport,medication,now,result,resultCriteria,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-12 09:10:00,"
            + "900,"
            + "117010004|POS,"          //URINE
            + "372755005|50|2018-02-16 09:10:00,"             //H2
            + "2018-02-18 09:10:00,"
            + "Medication Stewardship|fail,"
            + "H2 Blocker|Non-Compliant,"
            + "Antibiotic Administration|Non-Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
        List<Observation> obs = fhirService.listObservations(testModel.get(0).getPatient().getIdElement().getIdPart(), "http://cognitivemedicine.info", "med-stewardship-antibiotic");
        assertThat(obs.size(), is(1));
        assertThat(obs.get(0).getValueQuantity().getValue().doubleValue(), is(0.0));
    }

    @Test
    public void doMicrobiologyCulturesWithPositiveResultAndAntibiotics() throws FHIRException {

        String header = "id,birthDate,birthWeight,diagnosticReport,medication,medication,now,result,resultCriteria,resultCriteria";
        String data = ""
                + "1,"
                + "2018-02-12 09:10:00,"
                + "900,"
                + "117010004|POS,"          //URINE
                + "372755005|50|2018-02-16 09:10:00,"             //H2
                + "350121009|90|2018-02-16 09:10:00,"  // Anti-biotic
                + "2018-02-18 09:10:00,"
                + "Medication Stewardship|caution,"
                + "H2 Blocker|Non-Compliant,"
                + "Antibiotic Administration|Compliant";

        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
        List<Observation> obs = fhirService.listObservations(testModel.get(0).getPatient().getIdElement().getIdPart(), "http://cognitivemedicine.info", "med-stewardship-antibiotic");
        assertThat(obs.size(), is(1));
        assertThat(obs.get(0).getValueQuantity().getValue().doubleValue(), is(0.5));
    }

    @Test
    public void doPreexistingRuleResult() {
        String header = "id,birthDate,birthWeight,medication,observation,now,result,resultCriteria,resultCriteria";
        String data = ""
                + "1,"
                + "2018-02-12 09:10:00,"
                + "900,"
                + "372755005|50|2018-02-16 09:10:00,"             //H2
                + "med-stewardship-antibiotic|http://cognitivemedicine.info|Antibiotic Stewardship|0.5,"
                + "2018-02-18 09:10:00,"
                + "Medication Stewardship|caution,"
                + "H2 Blocker|Non-Compliant,"
                + "Antibiotic Administration|Compliant";

        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
}
