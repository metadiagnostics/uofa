/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.util.nec;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import com.cognitivemedicine.cs.abstractdecisioning.client.api.RestClient;
import com.cognitivemedicine.cs.abstractreasoning.data.Assessment;
import com.cognitivemedicine.cs.abstractreasoning.data.NECWorkProductModel;
import com.cognitivemedicine.cs.abstractreasoning.data.ProcessAdherenceServiceModel;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes;
import com.cognitivemedicine.cs.client.DataServiceRestClient;
import com.cognitivemedicine.sample.fhir.KTDFhirBaseService;
import com.cognitivemedicine.sample.test.util.RuleTestUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.ArrayList;
import java.util.List;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;

/**
 *
 * @author esteban
 */
public class NECTestExecutor {

    private KTDFhirBaseService fhirService;
    private RestClient outcomeService;
    private DataServiceRestClient simulatorService;

    public NECTestExecutor(KTDFhirBaseService fhirService, RestClient outcomeService,
                           DataServiceRestClient simulatorService) {
        this.fhirService = fhirService;
        this.outcomeService = outcomeService;
        this.simulatorService = simulatorService;
    }

    public void execute(KieBase kbase, NECTestModel model) {
        List<NECTestModel> models = new ArrayList<>();
        models.add(model);
        execute(kbase, models);
    }

    public void execute(KieBase kbase, List<NECTestModel> models) {

        for (NECTestModel model : models) {
            //create the session
            KieSession ksession = kbase.newKieSession();

            //Persist all the FHIR resources
            extractFHIRResources(model).forEach(r ->
                    fhirService.createOrUpdateResource(r));

            //Persist all Form Responses
            model.getFormResponses().forEach(fr                    -> {
                        try {
                            simulatorService.addFormResponse(RuleTestUtils.deserializeFormResponse(fr));
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                    }
            );

            //Execute
            ksession.insert("KTD-NEC-" + model.getNow().getAsString());
            RuleTestUtils.initKsession(ksession, fhirService, outcomeService, simulatorService);

            ksession.dispose();
            
            //Assert Results
            assertOutcome(model.getPatient().getIdElement().getIdPart(), model.getResults());
        }

    }

    private List<IBaseResource> extractFHIRResources(NECTestModel model) {

        List<IBaseResource> resources = new ArrayList<>();

        resources.add(model.getPatient());
        resources.add(model.getBirthWeight());
        resources.addAll(model.getDailyWeights());
        resources.addAll(model.getFeedings());
        resources.addAll(model.getNutritionOrders());
        resources.addAll(model.getObservations());
        resources.addAll(model.getProcedureRequests());
        resources.addAll(model.getDiagnosticReport());
        resources.addAll(model.getConditions());

        return resources;
    }

    private void assertOutcome(String patientId, List<NECTestModel.NECTestResult> results) {

        try {
            CdsWpOutcomes outcome = outcomeService.getDecisioningOutcomeByPatientIdAndKTDType(patientId, "NEC");
            assertNotNull("No CdsWpOutcome found for patient " + patientId, outcome);
            assertThat(outcome.getKtdReference(), is("NEC"));
            NECWorkProductModel modelPatient = RuleTestUtils.extractNECWorkProductModel(outcome);

            for (NECTestModel.NECTestResult result : results) {

                ProcessAdherenceServiceModel riskScoring = modelPatient.getProcessAdherence().stream()
                    .filter(p -> p.getLabel().equals(result.getKey())).findFirst().orElseThrow(() -> new IllegalStateException("Unable to find the expected result in the outcome: '" + result.getKey() + "'"));
                assertNotNull(riskScoring);
                assertThat(riskScoring.getLabel(), is(result.getKey()));
                assertThat(riskScoring.getStatus(), is(result.getStatus()));

                for (NECTestModel.NECTestResultCriteria criteria : result.getCriterias()) {
                    Assessment assessment = riskScoring.getCriteria().get(0).getData().stream()
                        .filter(a -> criteria.getName().equals(a.getName()))
                        .findFirst().orElseThrow(() -> new IllegalStateException("Criteria with name '" + criteria.getName() + "' not found in Result with key '" + result.getKey() + "'"));

                    assertThat(assessment.getName(), is(criteria.getName()));
                    assertThat(assessment.getCompliancy(), is(criteria.getCompliancy()));

                }
            }
        } catch (Exception e) {
            throw new IllegalStateException("Exception asserting test results", e);
        }
    }

}
