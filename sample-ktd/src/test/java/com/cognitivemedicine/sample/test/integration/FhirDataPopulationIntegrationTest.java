/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.integration;

import com.cognitivemedicine.sample.fhir.FhirBaseConfigurator;
import com.cognitivemedicine.sample.fhir.KTDFhirBaseService;
import com.cognitivemedicine.sample.test.util.FhirTestUtil;
import org.hl7.fhir.dstu3.model.*;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class FhirDataPopulationIntegrationTest {
    private KTDFhirBaseService fhirService;

    @Before
    public void doBefore() {
        fhirService = FhirBaseConfigurator.newInstance();
    }

    //@Test
    public void insertPatientData() {
        Patient patient = this.buildPatientDemographics();
        fhirService.createOrUpdateResource(patient);

        Practitioner practitioner = this.buildPractitioner();
        fhirService.createOrUpdateResource(practitioner);

        Organization gastroDeptOrganization = this.buildDeptOrganization("org-1", "Gastroenterology");
        fhirService.createOrUpdateResource(gastroDeptOrganization);

        Organization providerOrganization = this.buildProviderOrganization();
        fhirService.createOrUpdateResource(providerOrganization);

        Organization labServicesDept = this.buildDeptOrganization("org-3", "Lab Services");
        fhirService.createOrUpdateResource(labServicesDept);

        Organization imagingDept = this.buildDeptOrganization("org-4", "Imaging");
        fhirService.createOrUpdateResource(imagingDept);

        PractitionerRole role = this.buildPractitionerRole(practitioner, gastroDeptOrganization);
        fhirService.createOrUpdateResource(role);

        // For patient demographics we need:
        Observation cw = this.getCalculatedWeight(patient); // Insert or ask Emory to provide
        Observation ega = this.getEga(patient); // provided on spreadsheet
        Observation birthWeight = this.getBirthWeight(patient); // provided on spreadsheet
        Observation dailyWeight = this.getDailyWeight(patient); // provided on spreadsheet

        fhirService.createOrUpdateResource(cw);
        fhirService.createOrUpdateResource(ega);
        fhirService.createOrUpdateResource(birthWeight);
        fhirService.createOrUpdateResource(dailyWeight);

        Condition condition = this.buildCondition(patient, practitioner);
        fhirService.createOrUpdateResource(condition);

        DiagnosticReport labReport = this.buildDiagnosticReport(patient, labServicesDept,
                providerOrganization, practitioner, "Complete blood count (hemogram) panel",
                "dr-1", "HM");
        fhirService.createOrUpdateResource(labReport);

        DiagnosticReport report = this.buildDiagnosticReport(patient, imagingDept,
                providerOrganization, practitioner, "Chest X-Ray", "dr-2", "IMG");
        fhirService.createOrUpdateResource(report);
    }

    private Patient buildPatientDemographics() {
        Patient patient = new Patient();
        patient.setActive(true);
        patient.setId("TestPatient-1234");

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -5);

        List<Identifier> idList = new ArrayList<>();
        Identifier mrNumber = new Identifier();
        mrNumber.setType(FhirTestUtil.buildCodeableConcept("http://hl7.org/fhir/v2/0203", "MR", ""));
        mrNumber.setSystem("urn:oid:1.2.3.4");
        mrNumber.setValue("1234");
        idList.add(mrNumber);

        List<Address> addressList = new ArrayList<>();
        Address address = new Address();
        address.addLine("1111 Main St");
        address.setCity("San Diego");
        address.setPostalCode("92121");
        address.setState("CA");
        addressList.add(address);

        // Fields to add to spreadsheet or hardcode
        patient.setBirthDate(cal.getTime()); // Birth date
        patient.setName(this.buildName("Hollie", "Jones")); // First and last name
        patient.setGender(Enumerations.AdministrativeGender.FEMALE); // Gender
        patient.setIdentifier(idList); // Medical record number
        patient.setAddress(addressList); // Address

        return patient;
    }

    private Practitioner buildPractitioner() {
        Practitioner p = new Practitioner();
        p.setId("prac-1");
        p.setName(this.buildName("Adam", "Jones"));

        return p;
    }

    private PractitionerRole buildPractitionerRole(Practitioner practitioner, Organization organization) {
        Reference pracRef = new Reference("Practitioner/" + practitioner.getIdElement().getIdPart());
        pracRef.setResource(practitioner);

        Reference orgRef = new Reference("Organization/" + organization.getIdElement().getIdPart());
        orgRef.setResource(organization);

        PractitionerRole role = new PractitionerRole();
        role.setId("role-1");
        role.setPractitioner(pracRef);
        role.setOrganization(orgRef);
        return role;
    }

    private Organization buildDeptOrganization(String id, String name) {
        List<CodeableConcept> types = new ArrayList<>();
        types.add(FhirTestUtil.buildCodeableConcept("http://somesystem.com", "dept", "Department"));

        Organization org = new Organization();
        org.setId(id);
        org.setName(name);
        org.setType(types);
        return org;
    }

    private Organization buildProviderOrganization() {
        List<CodeableConcept> types = new ArrayList<>();
        types.add(FhirTestUtil.buildCodeableConcept("http://somesystem.com", "prov", "Provider"));

        Organization org = new Organization();
        org.setId("org-2");
        org.setName("Test Hospital");
        org.setType(types);
        return org;
    }

    private Observation getCalculatedWeight(Patient patient) {
        Observation cw = FhirTestUtil.buildObservation(patient, "cw-1", "9999999", "http://snomed.info/sct", "Calculated weight", 0.0);
        this.setObservationProperties(cw, 950, "grams", "http://unitsofmeasure.org", "g", patient.getBirthDate());
        return cw;
    }

    private Observation getEga(Patient patient) {
        Observation ega = FhirTestUtil.buildObservation(patient, "ega-1", "444135009", "http://snomed.info/sct", "Estimated fetal gestational age at delivery", 0.0);
        this.setObservationProperties(ega, 184, "days", "http://unitsofmeasure.org", "d", patient.getBirthDate());
        return ega;
    }

    private Observation getBirthWeight(Patient patient) {
        Observation bw = FhirTestUtil.buildObservation(patient, "bw-1", "364589006", "http://snomed.info/sct", "Birth weight", 0.0);
        this.setObservationProperties(bw, 950, "grams", "http://unitsofmeasure.org", "g", patient.getBirthDate());
        return bw;
    }

    private Observation getDailyWeight(Patient patient) {
        Observation dw = FhirTestUtil.buildObservation(patient, "dw-1", "27113001", "http://snomed.info/sct", "Body weight", 0.0);
        this.setObservationProperties(dw, 920, "grams", "http://unitsofmeasure.org", "g", patient.getBirthDate());
        return dw;
    }

    private Condition buildCondition(Patient patient, Practitioner practitioner) {
        Reference ref = new Reference("Patient/" + patient.getIdElement().getIdPart());
        ref.setResource(patient);

        Condition condition = new Condition();
        condition.setSubject(ref);
        condition.setId("condition-1");
        condition.setCode(FhirTestUtil.buildCodeableConcept("http://snomed.info/sct", "2707005", "Necrotizing enterocolitis in fetus OR newborn (disorder)"));

        Calendar onsetCal = Calendar.getInstance();
        onsetCal.add(Calendar.DATE, -3);

        Calendar reportedCal = Calendar.getInstance();
        reportedCal.add(Calendar.DATE, -2);

        CodeableConcept severity = FhirTestUtil.buildCodeableConcept("", "Severe", "Severe");

        Reference asserterRef = new Reference("Practitioner/" + practitioner.getIdElement().getIdPart());
        asserterRef.setResource(practitioner);

        condition.setOnset(new DateTimeType(onsetCal.getTime()));
        condition.setAssertedDateElement(new DateTimeType(reportedCal.getTime()));
        condition.setSeverity(severity);
        condition.setAsserter(asserterRef);

        return condition;
    }

    private DiagnosticReport buildDiagnosticReport(Patient patient, Organization deptOrg, Organization provOrg, Practitioner practitioner,
                                                   String codeText, String id, String categoryCode) {
        Reference ref = new Reference("Patient/" + patient.getIdElement().getIdPart());
        ref.setResource(patient);

        Reference deptOrgRef = new Reference("Organization/" + deptOrg.getIdElement().getIdPart());
        deptOrgRef.setResource(deptOrg);

        Reference provOrgRef = new Reference("Organization/" + provOrg.getIdElement().getIdPart());
        provOrgRef.setResource(provOrg);

        Reference provRef = new Reference("Practitioner/" + practitioner.getIdElement().getIdPart());
        provRef.setResource(practitioner);

        List<DiagnosticReport.DiagnosticReportPerformerComponent> performers = new ArrayList<>();
        DiagnosticReport.DiagnosticReportPerformerComponent perfOne = new DiagnosticReport.DiagnosticReportPerformerComponent();
        perfOne.setActor(deptOrgRef);
        performers.add(perfOne);

        CodeableConcept reportCode = FhirTestUtil.buildCodeableConcept("http://loinc.org", "58410-2", codeText);
        reportCode.setText(codeText);

        DiagnosticReport.DiagnosticReportPerformerComponent perfTwo = new DiagnosticReport.DiagnosticReportPerformerComponent();
        perfTwo.setActor(provOrgRef);
        performers.add(perfTwo);

        DiagnosticReport.DiagnosticReportPerformerComponent perfThree = new DiagnosticReport.DiagnosticReportPerformerComponent();
        perfThree.setActor(provRef);
        performers.add(perfThree);

        DiagnosticReport report = new DiagnosticReport();
        report.setId(id);
        report.setSubject(ref);
        report.setEffective(new DateTimeType(new Date()));

        report.setCode(reportCode);
        report.setStatus(DiagnosticReport.DiagnosticReportStatus.PARTIAL);
        report.setPerformer(performers);
        report.setCategory(FhirTestUtil.buildCodeableConcept("http://codesystem.org", categoryCode, categoryCode));

        return report;
    }

    private void setObservationProperties(Observation obs, long value, String unit, String system, String code, Date effective) {
        SimpleQuantity quantity = new SimpleQuantity();
        quantity.setValue(value);
        quantity.setUnit(unit);
        quantity.setSystem(system);
        quantity.setCode(code);
        obs.setValue(quantity);
        obs.setEffective(new DateTimeType(effective));
    }

    private List<HumanName> buildName(String firstName, String lastName) {
        List<HumanName> nameList = new ArrayList<>();
        HumanName name = new HumanName();
        List<StringType> givenNames = new ArrayList<>();
        givenNames.add(new StringType(firstName));
        name.setFamily(lastName);
        name.setGiven(givenNames);
        name.setText(lastName + ", " + firstName);
        nameList.add(name);
        return nameList;
    }
}
