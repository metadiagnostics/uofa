/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.util.nec;

import com.cognitivemedicine.cs.models.FormResponse;
import com.cognitivemedicine.sample.fhir.DiagnosticReportResult;
import com.cognitivemedicine.sample.model.Now;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.*;

/**
 *
 * @author esteban
 */
public class NECTestModel {

    public static class NECTestResult {
        private final String key;
        private final String status;
        
        private final List<NECTestResultCriteria> criterias = new ArrayList<>();

        public NECTestResult(String key, String status) {
            this.key = key;
            this.status = status;
        }

        public String getKey() {
            return key;
        }

        public String getStatus() {
            return status;
        }

        public boolean addCriteria(NECTestResultCriteria e) {
            return criterias.add(e);
        }
        
        public List<NECTestResultCriteria> getCriterias() {
            return criterias;
        }
        
    }
    
    public static class NECTestResultCriteria {
        private final String name;
        private final String compliancy;

        public NECTestResultCriteria(String name, String compliancy) {
            this.name = name;
            this.compliancy = compliancy;
        }

        public String getName() {
            return name;
        }

        public String getCompliancy() {
            return compliancy;
        }
            
    }
    
    private String id;
    private String description;
    private String resultLabel;
    private List<NECTestResult> results = new ArrayList<>();
    
    private Now now;
    
    private String predefinedPatientId;
    
    //FHIR Resources
    private Patient patient;
    
    private Observation birthWeight;
    private List<NutritionOrder> nutritionOrders = new ArrayList<>();
    private List<MedicationAdministration> feedings = new ArrayList<>();
    private List<Observation> dailyWeights = new ArrayList<>();
    private List<DiagnosticReport> diagnosticReports = new ArrayList<>();
    private List<Observation> observations = new ArrayList<>();
    private List<ProcedureRequest> procedureRequests = new ArrayList<>();
    private List<Condition> conditions = new ArrayList<>();
    
    //Forms
    private List<FormResponse> formResponses = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResultLabel() {
        return resultLabel;
    }

    public void setResultLabel(String resultLabel) {
        this.resultLabel = resultLabel;
    }

    public List<NECTestResult> getResults() {
        return results;
    }

    public void addResult(NECTestResult result) {
        this.results.add(result);
    }
    
    public void setResultCriterias(List<NECTestResult> results) {
        this.results = results;
    }

    public Now getNow() {
        return now;
    }

    public void setNow(Now now) {
        this.now = now;
    }

    public String getPredefinedPatientId() {
        return predefinedPatientId;
    }

    public void setPredefinedPatientId(String predefinedPatientId) {
        this.predefinedPatientId = predefinedPatientId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Observation getBirthWeight() {
        return birthWeight;
    }

    public void setBirthWeight(Observation birthWeight) {
        this.birthWeight = birthWeight;
    }

    public List<NutritionOrder> getNutritionOrders() {
        return nutritionOrders;
    }

    public void addNutritionOrder(NutritionOrder nutritionOrder) {
        this.nutritionOrders.add(nutritionOrder);
    }
    
    public void setNutritionOrders(List<NutritionOrder> nutritionOrders) {
        this.nutritionOrders = nutritionOrders;
    }

    public List<MedicationAdministration> getFeedings() {
        return feedings;
    }

    public void addFeeding(MedicationAdministration feeding) {
        this.feedings.add(feeding);
    }
    
    public void setFeedings(List<MedicationAdministration> feedings) {
        this.feedings = feedings;
    }

    public List<Observation> getDailyWeights() {
        return dailyWeights;
    }

    public void addDailyWeight(Observation dailyWeight) {
        this.dailyWeights.add(dailyWeight);
    }
    
    public void setDailyWeights(List<Observation> dailyWeights) {
        this.dailyWeights = dailyWeights;
    }

    public List<Observation> getObservations() {
        return this.observations;
    }

    public void setObservations(List<Observation> observations) {
        this.observations = observations;
    }

    public void addObservation(Observation observation) {
        this.observations.add(observation);
    }

    public List<DiagnosticReport> getDiagnosticReport() {
        return diagnosticReports;
    }

    public void addDiagnosticReport(DiagnosticReport diagnosticReport) {
        this.diagnosticReports.add(diagnosticReport);
    }
    
    public void setDiagnosticReport(List<DiagnosticReport> diagnosticReports) {
        this.diagnosticReports = diagnosticReports;
    }

    public List<ProcedureRequest> getProcedureRequests() {
        return procedureRequests;
    }

    public void setProcedureRequests(List<ProcedureRequest> procedureRequests) {
        this.procedureRequests = procedureRequests;
    }

    public void addProcedureRequest(ProcedureRequest procedureRequest) {
        this.procedureRequests.add(procedureRequest);
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public void addCondition(Condition condition) {
        this.conditions.add(condition);
    }
    
    public List<FormResponse> getFormResponses() {
        return formResponses;
    }

    public void addFormResponse(FormResponse formResponse) {
        this.formResponses.add(formResponse);
    }
    
    public void setFormResponses(List<FormResponse> formResponses) {
        this.formResponses = formResponses;
    }
    
}
