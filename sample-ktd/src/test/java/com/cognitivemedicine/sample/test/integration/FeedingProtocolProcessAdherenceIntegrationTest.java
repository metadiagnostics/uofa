/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.integration;

import com.cognitivemedicine.cs.abstractdecisioning.client.api.RestClient;
import com.cognitivemedicine.cs.client.DataServiceRestClient;
import com.cognitivemedicine.sample.fhir.FhirBaseConfigurator;
import com.cognitivemedicine.sample.fhir.KTDFhirBaseService;
import com.cognitivemedicine.sample.test.util.RuleTestUtils;
import com.cognitivemedicine.sample.test.util.nec.NECTestExecutor;
import com.cognitivemedicine.sample.test.util.nec.NECTestModel;
import com.cognitivemedicine.sample.test.util.nec.NECTestParser;
import java.util.List;
import java.util.UUID;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.kie.api.KieBase;

@Ignore("Integration tests shouldn't run on maven build")
public class FeedingProtocolProcessAdherenceIntegrationTest {

    
    private KieBase kbase;

    private KTDFhirBaseService fhirService;
    private RestClient outcomeService;
    private DataServiceRestClient simulatorService;

    @Before
    public void doBefore(){
        fhirService = FhirBaseConfigurator.newInstance();
        outcomeService = new RestClient();
        simulatorService = new DataServiceRestClient();

        kbase = RuleTestUtils.getKieBase("uofASampleDefaultKB");
    }
    
    @Test
    public void doEnteralFeedingInTheFirst72HoursTest(){
        String header = "id,birthDate,birthWeight,feeding,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-09 08:00:00,"                        //DOB
            + "900,"
            + "386127005|2|2018-02-09 18:00:00,"            //Formula (2 hours from DOB)
            + "2018-02-11 08:00:00,"
            + "Feeding Advancement|fail,"
            + "Feeding Advancement|Non-Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }

    @Test
    public void doNoEnteralFeedingInTheFirst72HoursTest(){
        String header = "id,birthDate,birthWeight,feeding,feeding,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-09 08:00:00,"                        //DOB
            + "900,"
            + "386127005|2|2018-02-09 18:00:00,"            //Formula (2 hours from DOB)
            + "386127005|1|2018-02-10 08:00:00,"            //Formula (24 hours from DOB)
            + "2018-02-11 08:00:00,"
            + "Feeding Advancement|caution,"
            + "Feeding Advancement|Partially Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doHistoricalEnteralFeedingInTheFirst72HoursTest(){
        String header = "id,patientId,birthDate,birthWeight,feeding,feeding,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "patient-1,"
            + "2018-02-09 08:00:00,"                        //DOB
            + "900,"
            + "386127005|2|2018-02-09 18:00:00,"            //Formula (2 hours from DOB)
            + "386127005|1|2018-02-10 08:00:00,"            //Formula (24 hours from DOB)
            + "2018-02-11 08:00:00,"                        //Today
            + "Feeding Advancement|caution,"
            + "Feeding Advancement|Partially Compliant\n";
        
        //The previous run should have generated an Observation for "trophic-feeding-history"
        //with a value of 1. After 72 hours, this 1 should remain.
        //A new run for the same patient (when he/she is more than 72 hours old)
        //should pickup this value.
        data += ""
            + "2,"
            + "patient-1,"
            + "2018-02-09 08:00:00,"                        //DOB
            + "900,"
            + ","
            + ","
            + "2018-02-15 08:00:00,"                        //Today
            + "Feeding Advancement|caution,"
            + "Feeding Advancement|Partially Compliant";
            
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doHistoricalEnteralFeedingUpdateInTheFirst72HoursTest(){
        String patientId = UUID.randomUUID().toString();
        String header = "id,patientId,birthDate,birthWeight,feeding,now,result,resultCriteria";
        String data = ""
            + "1,"
            + patientId + ","
            + "2018-02-09 08:00:00,"                        //DOB
            + "900,"
            + "386127005|2|2018-02-09 18:00:00,"            //Formula (2 hours from DOB)
            + "2018-02-11 08:00:00,"                        //Today
            + "Feeding Advancement|fail,"
            + "Feeding Advancement|Non-Compliant\n";
        
        //The previous run should have generated an Observation for "trophic-feeding-history"
        //with a value of 0. 
        
        //A new run (with a new Trophic Feeding) will now set a score of 1. The
        //existing trophic-feeding-history" will be updated with a value of 1.
        data += ""
            + "2,"
            + patientId + ","
            + "2018-02-09 08:00:00,"                        //DOB
            + "900,"
            + "386127005|1|2018-02-11 08:01:00,"            //Formula (48 hours from DOB)
            + "2018-02-11 09:00:00,"                        //Today
            + "Feeding Advancement|caution,"
            + "Feeding Advancement|Partially Compliant\n";
        
        
        //A new run for the same patient (when he/she is more than 72 hours old)
        //should pickup this value.
        data += ""
            + "2,"
            + patientId + ","
            + "2018-02-09 08:00:00,"                        //DOB
            + "900,"
            + ","
            + "2018-02-15 08:00:00,"                        //Today
            + "Feeding Advancement|caution,"
            + "Feeding Advancement|Partially Compliant";
            
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doNoActiveFeedingOrderTest(){
        String header = "id,birthDate,birthWeight,nutritionOrder,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-09 08:00:00,"                        //DOB
            + "900,"
            + "CANCELLED,"            
            + "2018-02-19 08:00:00,"
            + "Feeding Advancement|fail,"
            + "Feeding Advancement|Non-Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doActiveFeedingOrderTest(){
        String header = "id,birthDate,birthWeight,nutritionOrder,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-09 08:00:00,"                        //DOB
            + "900,"
            + "ACTIVE,"            
            + "2018-02-19 08:00:00,"
            + "Feeding Advancement|caution,"
            + "Feeding Advancement|Partially Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doCorrectionTest(){
        String header = "id,birthDate,birthWeight,feeding,feeding,nutritionOrder,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-09 08:00:00,"                        //DOB
            + "900,"
            + "386127005|2|2018-02-09 18:00:00,"            //Formula (2 hours from DOB)
            + "386127005|1|2018-02-10 08:00:00,"            //Formula (24 hours from DOB)
            + "ACTIVE,"            
            + "2018-02-11 08:00:00,"
            + "Feeding Advancement|ok,"
            + "Feeding Advancement|Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
    @Test
    public void doAverageFeedingVolumeTest(){
        String header = "id,birthDate,birthWeight,feeding,dailyWeight,feeding,dailyWeight,feeding,dailyWeight,feeding,dailyWeight,now,result,resultCriteria";
        String data = ""
            + "1,"
            + "2018-02-09 08:00:00,"                        //DOB
            + "900,"
            + "386127005|500|2018-02-14 08:00:00,"          //Feeding D-5
            + "900|2018-02-14 08:00:00,"                    //Weight D-5
            + "386127005|500|2018-02-15 08:00:00,"          //Feeding D-4
            + "900|2018-02-15 08:00:00,"                    //Weight D-4
            + "386127005|15700|2018-02-17 08:00:00,"        //Feeding D-2 
            + "1000|2018-02-17 08:00:00,"                   //Weight D-2
            + "386127005|15700|2018-02-18 08:00:00,"        //Feeding D-1  
            + "1000|2018-02-18 08:00:00,"                   //Weight D-1
            + "2018-02-19 08:00:00,"
            + "Feeding Advancement|caution,"
            + "Feeding Advancement|Partially Compliant";
        
        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }
    
}
