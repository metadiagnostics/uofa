/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.integration;

import com.cognitivemedicine.cs.abstractdecisioning.client.api.RestClient;
import com.cognitivemedicine.cs.client.DataServiceRestClient;
import com.cognitivemedicine.sample.fhir.FhirBaseConfigurator;
import com.cognitivemedicine.sample.fhir.KTDFhirBaseService;
import com.cognitivemedicine.sample.test.util.*;
import com.cognitivemedicine.sample.test.util.nec.NECTestExecutor;
import com.cognitivemedicine.sample.test.util.nec.NECTestModel;
import com.cognitivemedicine.sample.test.util.nec.NECTestParser;
import java.util.List;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.kie.api.KieBase;

@Ignore("Integration tests shouldn't run on maven build")
public class HumanMilkProcessAdherenceIntegrationTest {

    private KieBase kbase;

    private KTDFhirBaseService fhirService;
    private RestClient outcomeService;
    private DataServiceRestClient simulatorService;


    @Before
    public void doBefore(){
        fhirService = FhirBaseConfigurator.newInstance();
        outcomeService = new RestClient();
        simulatorService = new DataServiceRestClient();

        kbase = RuleTestUtils.getKieBase("uofASampleDefaultKB");
    }

    @Test
    public void doZeroPointsHumanMilkPATest() {
        String header = "id,birthDate,condition,birthWeight,feeding,now,result,resultCriteria";
        String data = ""
                + "1,"
                + "2018-02-09 08:00:00,"
                + "2707005|Necrotizing enterocolitis in fetus OR newborn (disorder)|2018-02-11 08:00:00,"
                + "900,"
                + "386127005|25,"                       //Formula
                + "2018-02-19 08:00:00,"
                + "Human Milk|fail,"
                + "Enteral Intake|Non-Compliant";

        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);

    }

    @Test
    public void doThreePointHumanMilkPATest() {

        String header = "id,birthDate,birthWeight,feeding,feeding,feeding,now,result,resultCriteria";
        String data = ""
                + "1,"
                + "2018-02-09 08:00:00,"
                + "900,"
                + "226790003|50,"                       //Mother
                + "7654321|25,"                         //Donor
                + "386127005|5,"                        //Formula
                + "2018-02-19 08:00:00,"
                + "Human Milk|caution,"
                + "Enteral Intake|Partially Compliant";

        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);

    }

    @Test
    public void doFourPointHumanMilkPATest() {

        String header = "id,birthDate,birthWeight,feeding,feeding,now,result,resultCriteria";
        String data = ""
                + "1,"
                + "2018-02-09 08:00:00,"
                + "900,"
                + "226790003|150,"                      //Mother
                + "386127005|5,"                        //Formula
                + "2018-02-19 08:00:00,"
                + "Human Milk|ok,"
                + "Enteral Intake|Compliant";

        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);

    }

    @Test
    public void doFivePointHumanMilkPATest() {
        String header = "id,birthDate,birthWeight,feeding,feeding,now,result,resultCriteria";
        String data = ""
                + "1,"
                + "2018-02-09 08:00:00,"
                + "900,"
                + "226790003|50,"                      //Mother
                + "226790003|50,"                      //Mother
                + "2018-02-19 08:00:00,"
                + "Human Milk|ok,"
                + "Enteral Intake|Compliant";

        List<NECTestModel> testModel = new NECTestParser().parse(header, data);
        new NECTestExecutor(fhirService, outcomeService, simulatorService).execute(kbase, testModel);
    }

}
