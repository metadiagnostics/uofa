/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.util;

import com.cognitivemedicine.cs.abstractdecisioning.client.api.RestClient;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toCollection;

/**
 *
 * @author esteban
 */
public class MockReasoningRestClient extends RestClient{

    private Map<String, List<CdsWpOutcomes>> outcomesByPatient = new HashMap<>();
    
    @Override
    public void addCdsWpOutcome(CdsWpOutcomes cdswpoutcomes) {
        outcomesByPatient.computeIfAbsent(cdswpoutcomes.getPatientId(), id -> new ArrayList<>()).add(cdswpoutcomes);
    }

    @Override
    public CdsWpOutcomes getDecisioningOutcomeByPatientIdAndKTDType(String patientId, String ktdReference) {
        return outcomesByPatient.getOrDefault(patientId, new ArrayList<>()).stream()
            .filter(o -> ktdReference.equals(o.getKtdReference()))
            .findAny().orElse(null);
    }

    @Override
    public void deleteCdsWpOutcome(String patientId, String ktdReference) {
        outcomesByPatient.put(
            patientId, 
            outcomesByPatient.getOrDefault(patientId, new ArrayList<>()).stream()
                .filter(o -> !ktdReference.equals(o.getKtdReference()))
                .collect(toCollection(() -> new ArrayList<>()))
        );
    }
    
    
    public List<CdsWpOutcomes> getCdsWpOutcomesByPatient(String patientId){
        return outcomesByPatient.getOrDefault(patientId, Collections.EMPTY_LIST);
    }
    
}
