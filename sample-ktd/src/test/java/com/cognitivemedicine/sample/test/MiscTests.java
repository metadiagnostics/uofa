/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.annotation.Child;
import ca.uhn.fhir.model.api.annotation.DatatypeDef;
import ca.uhn.fhir.parser.IParser;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.MedicationAdministration;
import org.hl7.fhir.dstu3.model.NutritionOrder;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Type;
import org.junit.Test;

/**
 * Some miscellaneous proof of concepts.
 * @author esteban
 */
public class MiscTests {

    @DatatypeDef(name = "expressionDate")
    public static class ExpressionDateType extends DateType {
        
        private String expression;

        public ExpressionDateType(String expression) {
            this.expression = expression;
        }
        
        public ExpressionDateType(Date theDate) {
            super(theDate);
        }

        @Override
        public String getValueAsString() {
            return this.expression;
        }
    }

    @DatatypeDef(name = "expressionDateTime")
    public static class ExpressionDateTimeType extends DateTimeType {

        private String expression;

        public ExpressionDateTimeType(String expression) {
            this.expression = expression;
        }
        
        public ExpressionDateTimeType(Date theDate) {
            super(theDate);
        }

        @Override
        public String getValueAsString() {
            return this.expression;
        }
        
    }

    @Test
    public void doTest() {
        Patient p = new Patient();
        p.setBirthDateElement(new ExpressionDateType(new Date()));
        replaceDateExpressions(p);

        MedicationAdministration ma = new MedicationAdministration();
        ma.setEffective(new ExpressionDateTimeType(new Date()));
        replaceDateExpressions(ma);

        Observation o = new Observation();
        o.setEffective(new ExpressionDateTimeType(new Date()));
        replaceDateExpressions(o);

        NutritionOrder no = new NutritionOrder();
        no.setDateTimeElement(new ExpressionDateTimeType(new Date()));
        replaceDateExpressions(no);

        System.out.println("END");
    }

    @Test
    public void doTest2() {
        Observation o = new Observation();
        o.setEffective(new ExpressionDateTimeType("N+10d"));
        
        IParser parser = FhirContext.forDstu3().newXmlParser();
        String xml = parser.encodeResourceToString(o);
        
        System.out.println("XML:\n"+xml);
    }
    
    private void replaceDateExpressions(Object src) {
        System.out.println("Class: " + src.getClass().getName());
        Field[] fields = src.getClass().getDeclaredFields();

        List<Field> dateTypeFields = new ArrayList<>();
        List<Field> dateTimeTypeFields = new ArrayList<>();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];

            if (field.isAnnotationPresent(Child.class)) {
                System.out.print("\tField: " + field.getName());
                Child annotation = field.getAnnotation(Child.class);

                if (annotation.type() != null) {

                    if (Arrays.stream(annotation.type())
                        .filter(t -> DateType.class.isAssignableFrom(t))
                        .count() > 0 && getFieldValueOrDie(src, field) instanceof ExpressionDateType) {
                        System.out.print(" (D)");
                        dateTypeFields.add(field);
                    }
                    if (Arrays.stream(annotation.type())
                        .filter(t -> DateTimeType.class.isAssignableFrom(t))
                        .count() > 0 && getFieldValueOrDie(src, field) instanceof ExpressionDateTimeType) {
                        System.out.print(" (DT)");
                        dateTimeTypeFields.add(field);
                    }

                }

                System.out.println("");
            }

        }

        for (Field f : dateTypeFields) {
            System.out.println("Date Field: " + f.getName());

            ExpressionDateType oldValue = (ExpressionDateType) getFieldValueOrDie(src, f);

            //evaluate oldValue expression
            DateType newValue = new DateType(oldValue.getValue());
            setFieldValueOrDie(src, f, newValue);
        }

        for (Field f : dateTimeTypeFields) {
            System.out.println("DateTime Field: " + f.getName());

            ExpressionDateTimeType oldValue = (ExpressionDateTimeType) getFieldValueOrDie(src, f);

            //evaluate oldValue expression
            DateTimeType newValue = new DateTimeType(oldValue.getValue());
            setFieldValueOrDie(src, f, newValue);
        }
    }

    private Object getFieldValueOrDie(Object o, Field f) {
        String[] suffixes = new String[]{"Element", "DateType", "DateTimeType"};

        for (int i = 0; i < suffixes.length; i++) {
            String suffix = suffixes[i];

            try {

                String name = StringUtils.capitalize(f.getName());
                String hasMethodName = "has" + name + suffix;
                String getMethodName = "get" + name + suffix;

                Method hasMethod = o.getClass().getDeclaredMethod(hasMethodName);
                Method getMethod = o.getClass().getDeclaredMethod(getMethodName);
                if (hasMethod != null && getMethod != null && ((Boolean) hasMethod.invoke(o)) == true) {
                    return getMethod.invoke(o);
                }
            } catch (NoSuchMethodException ex) {
            } catch (Exception ex) {
                throw new IllegalArgumentException("Unable to get value from field " + f.getName() + " from Object " + o, ex);
            }
        }
        return null;
    }

    private Object setFieldValueOrDie(Object o, Field f, Object value) {
        String[] suffixes = new String[]{"Element", ""};
        Class[] argumentTypes = new Class[]{value.getClass(), Type.class};
        for (int i = 0; i < suffixes.length; i++) {
            String suffix = suffixes[i];

            try {

                String name = StringUtils.capitalize(f.getName());
                String setMethodName = "set" + name + suffix;

                Method setMethod = o.getClass().getDeclaredMethod(setMethodName, argumentTypes[i]);
                if (setMethod != null) {
                    return setMethod.invoke(o, value);
                }
            } catch (NoSuchMethodException ex) {
            } catch (Exception ex) {
                throw new IllegalArgumentException("Unable to set value for field " + f.getName() + " from Object " + o, ex);
            }
        }
        return null;
    }
}
