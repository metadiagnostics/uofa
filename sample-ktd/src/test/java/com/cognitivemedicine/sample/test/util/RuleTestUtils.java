/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.util;

import com.cognitivemedicine.cdps.ktd.commons.logging.DefaultWorkingMemoryLogger;
import com.cognitivemedicine.cs.abstractdecisioning.client.api.RestClient;
import com.cognitivemedicine.cs.abstractreasoning.data.NECWorkProductModel;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes;
import com.cognitivemedicine.cs.client.DataServiceRestClient;
import com.cognitivemedicine.cs.models.FormResponse;
import com.cognitivemedicine.cs.models.FormResponseItem;
import com.cognitivemedicine.sample.fhir.KTDFhirBaseService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class RuleTestUtils {
    protected final static Logger LOG = LoggerFactory.getLogger(RuleTestUtils.class);

    public static KieBase getKieBase(String kbaseName) {
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks.getKieClasspathContainer();
        Results results = kContainer.verify(kbaseName);

        if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)) {
            List<Message> messages = results.getMessages(Message.Level.WARNING, Message.Level.ERROR);
            for (Message message : messages) {
                String out = String.format("[%s] - %s[%s,%s]: %s", message.getLevel(), message.getPath(), message.getLine(), message.getColumn(), message.getText());
                if (null== message.getLevel())                {
                    LOG.info(out);
                }
                else switch (message.getLevel()) {
                    case WARNING:
                        LOG.warn(out);
                        break;
                    case ERROR:
                        LOG.error(out);
                        break;
                    default:
                        LOG.info(out);
                        break;
                }
            }

            throw new IllegalStateException("Compilation errors were found. Check the logs.");
        }

        return kContainer.getKieBase(kbaseName);
    }

    public static void initKsession(KieSession ksession, KTDFhirBaseService fhirBaseService, RestClient outcomeService, DataServiceRestClient simulatorService){
        initKsession(ksession, fhirBaseService, outcomeService, simulatorService, true);
    }
    
    public static void initKsession(KieSession ksession, KTDFhirBaseService fhirBaseService, RestClient outcomeService, DataServiceRestClient simulatorService, boolean logsEnabled){

        if (logsEnabled){
            new DefaultWorkingMemoryLogger(DefaultWorkingMemoryLogger.LogLevel.TRACE, (org.drools.core.WorkingMemory)ksession);
        }

        ksession.setGlobal("fhirBaseService", fhirBaseService);
        ksession.setGlobal("outcomeService", outcomeService);
        ksession.setGlobal("simulatorService", simulatorService);

        //To avoid communication with external services, we do not execute
        //the PM-INIT phase
        //ksession.getAgenda().getAgendaGroup("PM-INIT").setFocus();
        //ksession.fireAllRules();
        ksession.getAgenda().getAgendaGroup("PM-START").setFocus();
        ksession.fireAllRules();
    }

    public static NECWorkProductModel extractNECWorkProductModel(CdsWpOutcomes outcome) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(outcome.getWorkProduct(), NECWorkProductModel.class);
    }

    public static FormResponse serializeFormResponse(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(json, FormResponse.class);
    }

    public static String deserializeFormResponse(FormResponse response) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(response);
    }
}
