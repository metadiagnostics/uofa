/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.test.integration;

import com.cognitivemedicine.cdps.ktd.commons.logging.DefaultWorkingMemoryLogger;
import com.cognitivemedicine.cdps.ktd.commons.logging.DefaultWorkingMemoryLogger.LogLevel;
import com.cognitivemedicine.cs.abstractdecisioning.client.api.RestClient;
import com.cognitivemedicine.cs.abstractreasoning.data.NECWorkProductModel;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes;
import com.cognitivemedicine.cs.client.DataServiceRestClient;
import com.cognitivemedicine.sample.fhir.FhirBaseConfigurator;
import com.cognitivemedicine.sample.fhir.KTDFhirBaseService;
import com.cognitivemedicine.sample.outcome.OutcomeRestClientConfigurator;
import com.cognitivemedicine.sample.test.util.MockSimulatorRestClient;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 *
 * @author esteban
 */
public class KTDIntegration {
    
    protected final static Logger LOG = LoggerFactory.getLogger(KTDIntegration.class);
    
    private KieSession ksession;
    
    @Before
    public void doBefore(){
        KieBase kbase = this.getKieBase("uofASampleDefaultKB");
        ksession = kbase.newKieSession();
        ksession.setGlobal("ktiId", UUID.randomUUID().toString());
    }
    
    public void doTest() throws IOException{
        KTDFhirBaseService fhirService = FhirBaseConfigurator.newInstance();
        RestClient outcomeService = OutcomeRestClientConfigurator.newInstance();
        MockSimulatorRestClient simulatorService = new MockSimulatorRestClient();     
//        Patient patient1 = new Patient();
//        patient1.setActive(true);
//        patient1 = fhirService.createOrUpdate(patient1);
//        
//        RiskFactors rf11 = new RiskFactors("RF 1 Patient 1", "High");
//        RiskFactors rf21 = new RiskFactors("RF 2 Patient 1", "High");
//        fhirService.save(patient1.getId(), rf11, rf21);
//        
//        ProcessAdherenceServiceModel pa11 = new ProcessAdherenceServiceModel("Process Adherence 1 Patient 1", "Red", Arrays.asList(new Criteria()));
//        fhirService.save(patient1.getId(), pa11);
//        
//        
//        Patient patient2 = new Patient();
//        patient2.setActive(true);
//        patient2 = fhirService.createOrUpdate(patient2);
//        
//        RiskFactors rf12 = new RiskFactors("RF 1 Patient 2", "Medium");
//        fhirService.save(patient2.getId(), rf12);
//        
//        ProcessAdherenceServiceModel pa12 = new ProcessAdherenceServiceModel("Process Adherence 1 Patient 2", "Red", Arrays.asList(new Criteria()));
//        ProcessAdherenceServiceModel pa22 = new ProcessAdherenceServiceModel("Process Adherence 2 Patient 2", "Green", Arrays.asList(new Criteria()));
//        fhirService.save(patient2.getId(), pa12, pa22);
        
        initKsession(fhirService, outcomeService, simulatorService);
        
        assertThat(outcomeService.getDecisioningOutcomeByPatientIdAndKTDType("PatientId-1234", "NEC").getIdcdsWpOutcomes(), notNullValue());
        
        //assertThat(outcomeService.getCdsWpOutcomesByPatient(patient1.getId()).size(), is(1));
        //assertThat(outcomeService.getCdsWpOutcomesByPatient(patient1.getId()).get(0).getKtdReference(), is("NEC"));
        NECWorkProductModel modelPatient1 = extractNECWorkProductModel(outcomeService.getDecisioningOutcomeByPatientIdAndKTDType("PatientId-1234", "NEC"));
        assertThat(modelPatient1.getRiskFactors().size(), is(3));
        assertThat(modelPatient1.getProcessAdherence().size(), is(4));
        
        
    }
    
    
    private void initKsession(KTDFhirBaseService fhirBaseService, RestClient outcomeService, DataServiceRestClient simulatorService){
        
        new DefaultWorkingMemoryLogger(LogLevel.TRACE, (org.drools.core.WorkingMemory)ksession);
        
        ksession.setGlobal("fhirBaseService", fhirBaseService);
        ksession.setGlobal("outcomeService", outcomeService);
        ksession.setGlobal("simulatorService", simulatorService);
        
        //To avoid communication with external services, we do not execute
        //the PM-INIT phase
        //ksession.getAgenda().getAgendaGroup("PM-INIT").setFocus();
        //ksession.fireAllRules();
        ksession.getAgenda().getAgendaGroup("PM-START").setFocus();
        ksession.fireAllRules();
    }

    private KieBase getKieBase(String kbaseName) {
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks.getKieClasspathContainer();
        Results results = kContainer.verify(kbaseName);

        if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)) {
            List<Message> messages = results.getMessages(Message.Level.WARNING, Message.Level.ERROR);
            for (Message message : messages) {
            	String out = String.format("[%s] - %s[%s,%s]: %s", message.getLevel(), message.getPath(), message.getLine(), message.getColumn(), message.getText());
            	if (null== message.getLevel())                {
                    LOG.info(out);
                }
            	else switch (message.getLevel()) {
                    case WARNING:
                        LOG.warn(out);
                        break;
                    case ERROR:
                        LOG.error(out);
                        break;
                    default:
                        LOG.info(out);
                        break;
                }
            }

            throw new IllegalStateException("Compilation errors were found. Check the logs.");
        }

        return kContainer.getKieBase(kbaseName);
    }
    
    private NECWorkProductModel extractNECWorkProductModel(CdsWpOutcomes outcome) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(outcome.getWorkProduct(), NECWorkProductModel.class);
    }
    
}
