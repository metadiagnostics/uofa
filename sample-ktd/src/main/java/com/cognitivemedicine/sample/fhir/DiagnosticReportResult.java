/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.fhir;

import java.util.Date;

public class DiagnosticReportResult {
    private String patientId;
    private String procedureRequestCodeSystem;
    private String procedureRequestCode;
    private String resultInterpretationCodeSystem;
    private String resultInterpretationCode;

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getProcedureRequestCodeSystem() {
        return procedureRequestCodeSystem;
    }

    public void setProcedureRequestCodeSystem(String procedureRequestCodeSystem) {
        this.procedureRequestCodeSystem = procedureRequestCodeSystem;
    }

    public String getProcedureRequestCode() {
        return procedureRequestCode;
    }

    public void setProcedureRequestCode(String procedureRequestCode) {
        this.procedureRequestCode = procedureRequestCode;
    }

    public String getResultInterpretationCodeSystem() {
        return resultInterpretationCodeSystem;
    }

    public void setResultInterpretationCodeSystem(String resultInterpretationCodeSystem) {
        this.resultInterpretationCodeSystem = resultInterpretationCodeSystem;
    }

    public String getResultInterpretationCode() {
        return resultInterpretationCode;
    }

    public void setResultInterpretationCode(String resultInterpretationCode) {
        this.resultInterpretationCode = resultInterpretationCode;
    }
}
