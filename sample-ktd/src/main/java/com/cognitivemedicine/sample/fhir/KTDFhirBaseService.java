/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.fhir;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.gclient.ReferenceClientParam;
import ca.uhn.fhir.rest.gclient.TokenClientParam;
import com.cognitivemedicine.cdsp.fhir.client.BaseService;
import com.cognitivemedicine.cdsp.fhir.client.config.FhirConfigurator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static java.util.stream.Collectors.toList;

import org.hl7.fhir.dstu3.model.*;

/**
 * This class is a specialization of {@link BaseService} containing extra methods
 * required by this KTD.
 * 
 * @author esteban
 */
public class KTDFhirBaseService extends BaseService {
    
    public KTDFhirBaseService(IGenericClient client) {
        super(client);
    }

    public KTDFhirBaseService(FhirConfigurator configurator) {
        super(configurator);
    }
   
    public List<Patient> listActivePatients() {
        Bundle bundle = this.getClient().search().forResource(Patient.class)
            .where(new TokenClientParam("active").exactly().code("true"))
            .count(50)
            .returnBundle(Bundle.class)
            .execute();
        
        return this.buildAndRetrieveAllResources(bundle, Patient.class);
    }
    
    public List<MedicationAdministration> listMedicationAdministrations(String patientId, String codeSystem, String code) {
        Bundle bundle = this.getClient().search().forResource(MedicationAdministration.class)
                .where(new ReferenceClientParam("subject").hasId(patientId))
                .where(new TokenClientParam("code").exactly().systemAndCode(codeSystem, code))
                .include(new Include("MedicationAdministration:subject"))
                .returnBundle(Bundle.class)
                .execute();

        return this.buildAndRetrieveAllResources(bundle, MedicationAdministration.class);
    }

    public List<Observation> listObservations(String patientId, String codeSystem, String code) {
        Bundle bundle = this.getClient().search().forResource(Observation.class)
                .where(new ReferenceClientParam("subject").hasId(patientId))
                .where(new TokenClientParam("code").exactly().systemAndCode(codeSystem, code))
                .include(new Include("Observation:subject"))
                .returnBundle(Bundle.class)
                .execute();

        return this.buildAndRetrieveAllResources(bundle, Observation.class);
    }
    
    public List<NutritionOrder> listNutritionOrders(String patientId) {
        Bundle bundle = this.getClient().search().forResource(NutritionOrder.class)
                .where(new ReferenceClientParam("patient").hasId(patientId))
                .include(new Include("NutritionOrder:patient"))
                .returnBundle(Bundle.class)
                .execute();

        return this.buildAndRetrieveAllResources(bundle, NutritionOrder.class);
    }

    public List<DiagnosticReport> listDiagnosticReports(String patientId) {
        Bundle bundle = this.getClient().search().forResource(DiagnosticReport.class)
                .include(new Include("DiagnosticReport:result"))
                .include(new Include("DiagnosticReport:subject"))
                .include(new Include("DiagnosticReport:based-on"))
                .where(new ReferenceClientParam("subject").hasId(patientId))
                .returnBundle(Bundle.class)
                .execute();

        return this.buildAndRetrieveAllResources(bundle, DiagnosticReport.class);
    }

    public void insertMedicationAntibioticObservation(Patient patient, double value) {
        Observation obs = this.buildMedicationAntibioticObservation(patient, value);
        this.getClient().create().resource(obs).execute();
    }
    
    public void createOrUpdateTrophicFeedingObservation(Patient patient, double value) {
        
        List<Observation> observations = this.listObservations(patient.getIdElement().getIdPart(), "http://cognitivemedicine.info", "X-trophic-feeding-history");
        
        if (observations.size() > 1){
            throw new IllegalStateException("More than 1 Trophic Feeding Observations found for patient "+patient.getId());
        }
        
        Observation obs;
        if (observations.isEmpty()){
            //Create a new Observation
            obs = this.buildTrophicFeedingObservation(patient, value);
        } else {
            //Update the existing Observation
            obs = observations.get(0);
            obs.setValue(new SimpleQuantity().setValue(value));
        }
        
        //Persist
        this.createOrUpdateResource(obs);
    }

    protected Observation buildMedicationAntibioticObservation(Patient patient, double value) {
        Observation obs = new Observation();

        Reference ref = new Reference("Patient/" + patient.getIdElement().getIdPart());
        ref.setResource(patient);
        obs.setSubject(ref);

        CodeableConcept codeableConcept = new CodeableConcept();
        List<Coding> codings = new ArrayList<>();
        codings.add(new Coding("http://cognitivemedicine.info", "med-stewardship-antibiotic", "Antibiotic Stewardship Score"));
        codeableConcept.setCoding(codings);
        obs.setCode(codeableConcept);

        Quantity quantity = new SimpleQuantity();
        quantity.setValue(value);
        obs.setValue(quantity);

        return obs;
    }
    
    protected Observation buildTrophicFeedingObservation(Patient patient, double value) {
        Observation obs = new Observation();
        Reference ref = new Reference("Patient/" + patient.getIdElement().getIdPart());
        ref.setResource(patient);
        obs.setSubject(ref);

        CodeableConcept codeableConcept = new CodeableConcept();
        List<Coding> codings = new ArrayList<>();
        codings.add(new Coding("http://cognitivemedicine.info", "X-trophic-feeding-history", "Trophic Feeding"));
        codeableConcept.setCoding(codings);
        obs.setCode(codeableConcept);

        Quantity quantity = new SimpleQuantity();
        quantity.setValue(value);
        obs.setValue(quantity);

        return obs;
    }

    private <T> List<T> buildAndRetrieveAllResources(Bundle bundle, Class<T> resourceType) {
        if (bundle.isEmpty()){
            return Collections.EMPTY_LIST;
        }

        List<T> resourceList = new ArrayList<>(this.getResourceFromBundle(bundle, resourceType));

        boolean hasLink = bundle.hasLink() && bundle.getLink("next") != null;
        while (hasLink) {
            bundle = this.fetchNextPage(bundle.getLink("next").getUrl());
            resourceList.addAll(this.getResourceFromBundle(bundle, resourceType));
            hasLink = bundle.hasLink() && bundle.getLink("next") != null;
        }

        return resourceList;
    }

    private Bundle fetchNextPage(String url) {
        return this.getClient().search().byUrl(url)
                .returnBundle(Bundle.class)
                .execute();
    }

    private <T> List<T> getResourceFromBundle(Bundle bundle, Class<T> resourceType) {
        return bundle.getEntry().stream()
                .map(e -> e.getResource())
                .filter(resourceType::isInstance)
                .map(resourceType::cast)
                .collect(toList());
    }

}
