/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.util;

import com.cognitivemedicine.cs.abstractreasoning.data.Assessment;
import com.cognitivemedicine.cs.abstractreasoning.data.Criteria;
import com.cognitivemedicine.cs.abstractreasoning.data.ProcessAdherenceServiceModel;
import com.cognitivemedicine.sample.model.OutcomeBundle;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ProcessAdherenceUtil {
    public static final String STATUS_FAIL = "fail";
    public static final String STATUS_CAUTION = "caution";
    public static final String STATUS_OK = "ok";
    public static final String COMPLIANCY_COMPLIANT = "Compliant";
    public static final String COMPLIANCY_PARTIALLY_COMPLIANT = "Partially Compliant";
    public static final String COMPLIANCY_NON_COMPLIANT = "Non-Compliant";
    public static final String HUMAN_MILK = "Human Milk";
    public static final String RISK_SCORING = "Risk Scoring";
    public static final String MEDICATION_STEWARDSHIP = "Medication Stewardship";
    public static final String FEEDING_ADVANCEMENT = "Feeding Advancement";
    public static final String CRITERIA_H2_BLOCKER = "H2 Blocker";
    public static final String CRITERIA_ANTIBIOTIC_ADMINISTRATION = "Antibiotic Administration";

    public static void convertScoresToProcessAdherenceModel(OutcomeBundle bundle) {
        List<ProcessAdherenceServiceModel> models = bundle.getModel().getProcessAdherence();

        // TODO: Do we need to add Records if none of our rules are matched?
        if (bundle.getProcessAdherenceScores().containsKey(HUMAN_MILK)) {
            models.add(convertHumanMilk(bundle.getProcessAdherenceScores().get(HUMAN_MILK)));
        }

        if (bundle.getProcessAdherenceScores().containsKey(RISK_SCORING)) {
            models.add(convertRiskScoring(bundle.getProcessAdherenceScores().get(RISK_SCORING)));
        }
        if (bundle.getProcessAdherenceScores().containsKey(FEEDING_ADVANCEMENT)) {
            models.add(convertFeedingAdvancementScoring(bundle.getProcessAdherenceScores().get(FEEDING_ADVANCEMENT)));
        }

        models.add(convertMedicationStewardship(bundle.getProcessAdherenceCriteria().get(MEDICATION_STEWARDSHIP)));
    }

    private static ProcessAdherenceServiceModel convertHumanMilk(Double score) {
        String status;
        String compliancy;

        if (score <= 1) {
            status = STATUS_FAIL;
            compliancy = COMPLIANCY_NON_COMPLIANT;
        } else if (score <= 3) {
            status = STATUS_CAUTION;
            compliancy = COMPLIANCY_PARTIALLY_COMPLIANT;
        } else {
            status = STATUS_OK;
            compliancy = COMPLIANCY_COMPLIANT;
        }

        return new ProcessAdherenceServiceModel(HUMAN_MILK, status, buildSingleCriteriaList(compliancy, "Enteral Intake"));
    }

    private static ProcessAdherenceServiceModel convertRiskScoring(Double score) {
        String status;
        String compliancy;

        if (score >= 1) {
            status = STATUS_OK;
            compliancy = COMPLIANCY_COMPLIANT;
        } else if (score < 1 && score >= .5) {
            status = STATUS_CAUTION;
            compliancy = COMPLIANCY_PARTIALLY_COMPLIANT;
        } else {
            status = STATUS_FAIL;
            compliancy = COMPLIANCY_NON_COMPLIANT;
        }


        return new ProcessAdherenceServiceModel(RISK_SCORING, status, buildSingleCriteriaList(compliancy, "GutCheckNEC Scoring"));
    }

    private static ProcessAdherenceServiceModel convertMedicationStewardship(Map<String, Double> criteria) {
        Date today = new Date();
        List<Assessment> assessmentList = new ArrayList<>();
        double score = 0;

        if (criteria != null && criteria.containsKey(CRITERIA_H2_BLOCKER) && criteria.get(CRITERIA_H2_BLOCKER) >= .5) {
            score += .5;
            assessmentList.add(buildAssessment(today, COMPLIANCY_COMPLIANT, CRITERIA_H2_BLOCKER));
        } else {
            assessmentList.add(buildAssessment(today, COMPLIANCY_NON_COMPLIANT, CRITERIA_H2_BLOCKER));
        }

        if (criteria != null && criteria.containsKey(CRITERIA_ANTIBIOTIC_ADMINISTRATION) && criteria.get(CRITERIA_ANTIBIOTIC_ADMINISTRATION) >= .5) {
            score += .5;
            assessmentList.add(buildAssessment(today, COMPLIANCY_COMPLIANT, CRITERIA_ANTIBIOTIC_ADMINISTRATION));
        } else {
            assessmentList.add(buildAssessment(today, COMPLIANCY_NON_COMPLIANT, CRITERIA_ANTIBIOTIC_ADMINISTRATION));
        }

        String status;
        if (score >= 1) {
            status = STATUS_OK;
        } else if (score < 1 && score >= .5) {
            status = STATUS_CAUTION;
        } else {
            status = STATUS_FAIL;
        }

        List<Criteria> criteriaList = new ArrayList<>();
        criteriaList.add(buildCriteria(today, assessmentList));

        return new ProcessAdherenceServiceModel(MEDICATION_STEWARDSHIP, status, criteriaList);
    }
    
    private static ProcessAdherenceServiceModel convertFeedingAdvancementScoring(Double score) {
        String status;
        String compliancy;

        if (score >= 3) {
            status = STATUS_OK;
            compliancy = COMPLIANCY_COMPLIANT;
        } else if (score == 1 || score == 2) {
            status = STATUS_CAUTION;
            compliancy = COMPLIANCY_PARTIALLY_COMPLIANT;
        } else {
            status = STATUS_FAIL;
            compliancy = COMPLIANCY_NON_COMPLIANT;
        }


        return new ProcessAdherenceServiceModel(FEEDING_ADVANCEMENT, status, buildSingleCriteriaList(compliancy, "Feeding Advancement"));
    }

    private static List<Criteria> buildSingleCriteriaList(String compliancy, String assessmentName) {
        Date today = new Date();

        List<Criteria> criteriaList = new ArrayList<>();
        List<Assessment> assessments = new ArrayList<>();
        assessments.add(buildAssessment(today, compliancy, assessmentName));
        criteriaList.add(buildCriteria(today, assessments));
        return criteriaList;
    }

    private static Criteria buildCriteria(Date date, List<Assessment> assessments) {
        Criteria criteria = new Criteria();
        criteria.setDate(date);
        criteria.setType("dateTime");
        criteria.setData(assessments);
        return criteria;
    }

    private static Assessment buildAssessment(Date date, String compliancy, String name) {
        Assessment assessment = new Assessment();
        assessment.setDate(date);
        assessment.setCompliancy(compliancy);
        assessment.setName(name);
        return assessment;
    }
}
