/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.util;

import com.cognitivemedicine.sample.fhir.DiagnosticReportResult;
import com.cognitivemedicine.sample.model.Now;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataUtil {
    
    protected final static Logger LOG = LoggerFactory.getLogger(DataUtil.class);
    
    
    
    public static Integer getDaysOfLife(Date birthDate) {
        return DataUtil.getDaysOfLife(new Date(), birthDate);
    }
    
    public static Integer getDaysOfLife(Now today, Date birthDate) {
        return DataUtil.getDaysOfLife(today.getAsDate(), birthDate);
    }
    
    public static Integer getDaysOfLife(Date today, Date birthDate) {
        return (int) DataUtil.getHoursOfLife(today, birthDate)/24;
    }
    
    public static Integer getHoursOfLife(Now today, Date birthDate) {
        return DataUtil.getHoursOfLife(today.getAsDate(), birthDate);
    }
    
    public static Integer getHoursOfLife(Date today, Date birthDate) {
        return (int)( (today.getTime() - birthDate.getTime()) / (1000 * 60 * 60));
    }
    
    public static Date addTime(Now original, long amount, TemporalUnit unit){
        return addTime(original.getAsDate(), amount, unit);
    }
    
    public static Date addTime(Date original, long amount, TemporalUnit unit){
        Instant result = original.toInstant();
        result = result.plus(amount, unit);
        return Date.from(result);
    }
    
    public static Date formResponseToDate(String responseDate){
        try {
            return new SimpleDateFormat("yyyyMMddHHmm").parse(responseDate);
        } catch (ParseException ex) {
            LOG.error("Exception converting String '"+responseDate+"' as date. Returning null", ex);
            return null;
        }
    }

    public static List<DiagnosticReportResult> convertListToDiagnosticReportResultList(List<DiagnosticReport> diagnosticReportList) {
        List<DiagnosticReportResult> resultList = new ArrayList<>();

        for (DiagnosticReport report : diagnosticReportList) {
            String procRequestCode = null;
            String procRequestSystem = null;

            if (report.hasBasedOn()) {
                for (Reference basedOn : report.getBasedOn()) {
                    if (basedOn.getResource() != null && basedOn.getResource() instanceof ProcedureRequest) {
                        ProcedureRequest procRequest = (ProcedureRequest) basedOn.getResource();

                        if (procRequest.hasCode() && procRequest.getCode().hasCoding()) {
                            procRequestSystem = procRequest.getCode().getCodingFirstRep().getSystem();
                            procRequestCode = procRequest.getCode().getCodingFirstRep().getCode();
                        }
                    }
                }

                if (procRequestCode != null && report.hasResult()) {
                    Reference ref = report.getResultFirstRep();
                    if (ref.getResource() != null && ref.getResource() instanceof Observation) {
                        Observation obs = (Observation) ref.getResource();

                        if (obs.hasInterpretation() && obs.getInterpretation().hasCoding()) {
                            DiagnosticReportResult result = new DiagnosticReportResult();
                            result.setPatientId(((Patient) report.getSubject().getResource()).getIdElement().getIdPart());
                            result.setProcedureRequestCode(procRequestCode);
                            result.setProcedureRequestCodeSystem(procRequestSystem);
                            result.setResultInterpretationCode(obs.getInterpretation().getCodingFirstRep().getCode());
                            result.setResultInterpretationCodeSystem(obs.getInterpretation().getCodingFirstRep().getSystem());
                            resultList.add(result);
                        }
                    }
                }
            }
        }

        return resultList;
    }
}
