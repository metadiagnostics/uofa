/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.simulator;

import com.cognitivemedicine.config.utils.ConfigUtils;
import com.cognitivemedicine.cs.client.DataServiceRestClient;
import com.cognitivemedicine.sample.config.ConfigUtilsConstants;

public class SimulatorBaseConfigurator {

    public static DataServiceRestClient newInstance() {
        ConfigUtils config = ConfigUtils.getInstance(ConfigUtilsConstants.CONTEXT_NAME);

        String url = config.getString(ConfigUtilsConstants.KEY_SIMULATOR_URL, "http://localhost:8083/api");

        return new DataServiceRestClient(url);
    }
}
