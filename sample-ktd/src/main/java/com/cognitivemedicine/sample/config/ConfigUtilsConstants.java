/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.config;

/**
 *
 * @author esteban
 */
public class ConfigUtilsConstants {
    
    public static final String CONTEXT_NAME = "ktd.uofa.config";
    
    public static final String KEY_FHIR_AUTHENTICATION_TYPE = "fhir.authentication.type";
    public static final String KEY_FHIR_ENCODING = "fhir.encoding";
    public static final String KEY_FHIR_PRETTY_PRINT = "fhir.pretty.print";
    public static final String KEY_FHIR_PROXY = "fhir.proxy";
    public static final String KEY_FHIR_ROOT_URL = "fhir.root.url";
    public static final String KEY_FHIR_VALIDATE_CONFORMANCE = "fhir.validate.conformance";
    public static final String KEY_FHIR_VERSION = "fhir.version";
    
    public static final String KEY_OUTCOME_URL = "outcome.url";
    public static final String KEY_SIMULATOR_URL = "simulator.data.service.url";
    
}
