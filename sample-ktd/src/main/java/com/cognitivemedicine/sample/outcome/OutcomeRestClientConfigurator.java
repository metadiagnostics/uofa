/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.outcome;

import com.cognitivemedicine.config.utils.ConfigUtils;
import com.cognitivemedicine.cs.abstractdecisioning.client.api.RestClient;
import com.cognitivemedicine.sample.config.ConfigUtilsConstants;

/**
 * Class in charge of configuring instances of {@link RestClient} based
 * on config-utils.
 * 
 * @author esteban
 */
public class OutcomeRestClientConfigurator {

    public static RestClient newInstance() {
        ConfigUtils config = ConfigUtils.getInstance(ConfigUtilsConstants.CONTEXT_NAME);

        String url = config.getString(ConfigUtilsConstants.KEY_OUTCOME_URL, "http://localhost:8082");
        
        return new RestClient(url);
    }

}
