/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.model;

import com.cognitivemedicine.cs.abstractreasoning.data.NECWorkProductModel;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class holds together a {@link CdsWpOutcomes} and its corresponding
 * {@link NECWorkProductModel}
 * 
 * @author esteban
 */
public class OutcomeBundle {

    private final CdsWpOutcomes outcome;
    private final NECWorkProductModel model;

    // Stores process adherence records with single criteria
    private Map<String, Double> processAdherenceScores;
    // Stores process adherence records with multiple criteria
    private Map<String, Map<String, Double>> processAdherenceCriteria;

    public OutcomeBundle(CdsWpOutcomes outcome, NECWorkProductModel model) {
        this.outcome = outcome;
        this.model = model;
        this.processAdherenceScores = new HashMap<>();
        this.processAdherenceCriteria = new HashMap<>();
    }

    public CdsWpOutcomes getOutcome() {
        return outcome;
    }
    
    public NECWorkProductModel getModel() {
        return model;
    }
    
    public String getPatientId(){
        return outcome.getPatientId();
    }
    
    public Map<String, Double> getProcessAdherenceScores() {
        return this.processAdherenceScores;
    }
    
    public double getProcessAdherenceScore(String key) {
        return this.processAdherenceScores.getOrDefault(key, 0.0);
    }

    public Map<String, Map<String, Double>> getProcessAdherenceCriteria() {
        return this.processAdherenceCriteria;
    }

    public void setProcessAdherenceScore(String key, Double value) {
        this.processAdherenceScores.put(key, value);
    }
    
    public void addProcessAdherenceScore(String key, Double value) {
        Double newValue = this.processAdherenceScores.containsKey(key) ? this.processAdherenceScores.get(key) + value : value;
        this.processAdherenceScores.put(key, newValue);
    }
    
    public void addProcessAdherenceCriteria(String key, String criteriaName, Double criteriaValue) {
        if (!this.processAdherenceCriteria.containsKey(key)) {
            this.processAdherenceCriteria.put(key, new HashMap<>());
        }

        this.processAdherenceCriteria.get(key).put(criteriaName, criteriaValue);
    }
    
}
