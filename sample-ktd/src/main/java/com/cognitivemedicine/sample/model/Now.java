/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.sample.model;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Class that represents the current time in the session.
 * 
 * @author esteban
 */
public class Now {
    
    public final Instant now;

    public Now(Instant now) {
        this.now = now;
    }
    
    public Now(Date now) {
        this.now = now.toInstant();
    }
    
    /**
     * Creates a Now instance based on an Instant ISO String
     * @param now 
     * @see DateTimeFormatter#ISO_INSTANT
     * 
     */
    public Now(String now) {
        this.now = Instant.parse(now);
    }
    
    public Date getAsDate(){
        return Date.from(now);
    }
    
    public Instant getAsInstant(){
        return this.now;
    }
    
    public String getAsString(){
        return DateTimeFormatter.ISO_INSTANT.format(this.now);
    }
    
}
