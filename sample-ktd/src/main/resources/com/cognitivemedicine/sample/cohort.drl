package com.cognitivemedicine.sample;

import com.cognitivemedicine.cs.abstractreasoning.data.NECWorkProductModel;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes;

import com.cognitivemedicine.sample.fhir.KTDFhirBaseService;
import com.cognitivemedicine.sample.model.OutcomeBundle;


import java.util.List;

import org.hl7.fhir.dstu3.model.Patient
import java.util.ArrayList
import com.cognitivemedicine.cs.abstractreasoning.data.LineChartServiceModel;

global KTDFhirBaseService fhirBaseService;

rule "Fetch Patients"
agenda-group "PM-START"
when
then  
  List<Patient> patients = fhirBaseService.listActivePatients();
  for (Patient p : patients){
    insert(p);
  }
end

/**
When no OutcomeBundle for a patient exist in the session, a fresh one is created.
Other rules in other files will then complete the missing pieces of this fact.
**/
rule "Create new OutcomeBundle"
when
  $p: Patient()
  not CdsWpOutcomes(patientId == $p.idElement.getIdPart())
then
  CdsWpOutcomes outcome = new CdsWpOutcomes();
  outcome.setPatientId($p.getIdElement().getIdPart());
  outcome.setKtdReference("NEC");
  outcome.setDateProcessed(new java.util.Date());
  outcome.setCode("2707005");
  outcome.setCodeSystem("http://snomed.info/sct");
  
  NECWorkProductModel model = new NECWorkProductModel();
  model.setProcessAdherence(new ArrayList<>());
  model.setRiskFactors(new ArrayList<>());
  model.setLineChartModel(new LineChartServiceModel());
  
  insert(new OutcomeBundle(outcome, model));
  
end