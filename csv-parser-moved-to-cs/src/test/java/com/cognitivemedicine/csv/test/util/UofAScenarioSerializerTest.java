/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.test.util;

import com.cognitivemedicine.csv.processing.CodeableConceptSectionParser;
import com.cognitivemedicine.csv.processing.FHIRResourceSectionParser;
import com.cognitivemedicine.csv.processing.Parser;
import com.cognitivemedicine.csv.processing.PatientSectionParser;
import com.cognitivemedicine.csv.util.UofAScenarioSerializer;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static org.hamcrest.CoreMatchers.is;

import org.apache.commons.io.IOUtils;
import org.hl7.fhir.dstu3.model.BaseResource;
import org.hl7.fhir.dstu3.model.DiagnosticReport;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.ProcedureRequest;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class UofAScenarioSerializerTest {
    
    @Test
    public void serializeCSV1Test() throws Exception{
        InputStreamReader csv = new InputStreamReader(UofAScenarioSerializerTest.class.getResourceAsStream("/csv/csv-1.csv"));

        PatientSectionParser patientSectionParser = new PatientSectionParser();
        CodeableConceptSectionParser ccSectionParser = new CodeableConceptSectionParser();
        FHIRResourceSectionParser fhirResourceSectionParser = new FHIRResourceSectionParser(patientSectionParser, ccSectionParser);
        
        Parser parser = new Parser();
        parser.setSectionParsers(patientSectionParser, ccSectionParser, fhirResourceSectionParser);
        parser.parse(csv);
        List<BaseResource> fhirResources = parser.getFHIRResources();
        
        UofAScenarioSerializer serializer = new UofAScenarioSerializer();
        String yaml = serializer.toYaml(fhirResources);
        
        System.out.println("\nYAML:\n"+yaml+"\n\n");
        
    }
    
    /**
     * Open + Closed resource in a single column.
     * @throws Exception 
     */
    @Test
    public void serializeCSV2Test() throws Exception{
        InputStreamReader csv = new InputStreamReader(UofAScenarioSerializerTest.class.getResourceAsStream("/csv/csv-2.csv"));

        PatientSectionParser patientSectionParser = new PatientSectionParser();
        CodeableConceptSectionParser ccSectionParser = new CodeableConceptSectionParser();
        FHIRResourceSectionParser fhirResourceSectionParser = new FHIRResourceSectionParser(patientSectionParser, ccSectionParser);
        
        Parser parser = new Parser();
        parser.setSectionParsers(patientSectionParser, ccSectionParser, fhirResourceSectionParser);
        parser.parse(csv);
        List<BaseResource> fhirResources = parser.getFHIRResources();
        
        UofAScenarioSerializer serializer = new UofAScenarioSerializer();
        String yaml = serializer.toYaml(fhirResources);
        
        System.out.println("\nYAML:\n"+yaml+"\n\n");
        
        Map<? extends Class<? extends BaseResource>, List<BaseResource>> resourcesByClass = fhirResources.stream().collect(Collectors.groupingBy(r -> r.getClass()));
        
        assertThat(resourcesByClass.get(Patient.class).size(), is(1));
        assertThat(resourcesByClass.get(Observation.class).size(), is(4));
        assertThat(resourcesByClass.get(ProcedureRequest.class).size(), is(1));
        assertThat(resourcesByClass.get(DiagnosticReport.class).size(), is(1));
        
    }
    
    /**
     * Multiple Open + Closed resource in a single column.
     * @throws Exception 
     */
    @Test
    public void serializeCSV3Test() throws Exception{
        InputStreamReader csv = new InputStreamReader(UofAScenarioSerializerTest.class.getResourceAsStream("/csv/csv-3.csv"));

        PatientSectionParser patientSectionParser = new PatientSectionParser();
        CodeableConceptSectionParser ccSectionParser = new CodeableConceptSectionParser();
        FHIRResourceSectionParser fhirResourceSectionParser = new FHIRResourceSectionParser(patientSectionParser, ccSectionParser);
        
        Parser parser = new Parser();
        parser.setSectionParsers(patientSectionParser, ccSectionParser, fhirResourceSectionParser);
        parser.parse(csv);
        List<BaseResource> fhirResources = parser.getFHIRResources();
        
        UofAScenarioSerializer serializer = new UofAScenarioSerializer();
        String yaml = serializer.toYaml(fhirResources);
        
        System.out.println("\nYAML:\n"+yaml+"\n\n");
        
        Map<? extends Class<? extends BaseResource>, List<BaseResource>> resourcesByClass = fhirResources.stream().collect(Collectors.groupingBy(r -> r.getClass()));
        
        assertThat(resourcesByClass.get(Patient.class).size(), is(1));
        assertThat(resourcesByClass.get(Observation.class).size(), is(8));
        assertThat(resourcesByClass.get(ProcedureRequest.class).size(), is(2));
        assertThat(resourcesByClass.get(DiagnosticReport.class).size(), is(2));
        
    }
    
    /**
     * Multiple Open + Closed resource in a multiple columns.
     * @throws Exception 
     */
    @Test
    public void serializeCSV4Test() throws Exception{
        InputStreamReader csv = new InputStreamReader(UofAScenarioSerializerTest.class.getResourceAsStream("/csv/csv-4.csv"));

        PatientSectionParser patientSectionParser = new PatientSectionParser();
        CodeableConceptSectionParser ccSectionParser = new CodeableConceptSectionParser();
        FHIRResourceSectionParser fhirResourceSectionParser = new FHIRResourceSectionParser(patientSectionParser, ccSectionParser);
        
        Parser parser = new Parser();
        parser.setSectionParsers(patientSectionParser, ccSectionParser, fhirResourceSectionParser);
        parser.parse(csv);
        List<BaseResource> fhirResources = parser.getFHIRResources();
        
        UofAScenarioSerializer serializer = new UofAScenarioSerializer();
        String yaml = serializer.toYaml(fhirResources);
        
        System.out.println("\nYAML:\n"+yaml+"\n\n");
        
        Map<? extends Class<? extends BaseResource>, List<BaseResource>> resourcesByClass = fhirResources.stream().collect(Collectors.groupingBy(r -> r.getClass()));
        
        assertThat(resourcesByClass.get(Patient.class).size(), is(1));
        assertThat(resourcesByClass.get(Observation.class).size(), is(9));
        assertThat(resourcesByClass.get(ProcedureRequest.class).size(), is(3));
        assertThat(resourcesByClass.get(DiagnosticReport.class).size(), is(3));
        
        for (BaseResource baseResource : resourcesByClass.get(DiagnosticReport.class)) {
            DiagnosticReport dr = (DiagnosticReport) baseResource;
            System.out.println("DR "+dr.getIssued()+" is related to PR "+((ProcedureRequest)dr.getBasedOnFirstRep().getResource()).getAuthoredOn());
        }
        
    }
    
    @Test
    public void serializeMBMOnlyEATest() throws Exception{
        InputStreamReader csv = new InputStreamReader(UofAScenarioSerializerTest.class.getResourceAsStream("/csv/mbm-only-ea.csv"));

        PatientSectionParser patientSectionParser = new PatientSectionParser();
        CodeableConceptSectionParser ccSectionParser = new CodeableConceptSectionParser();
        FHIRResourceSectionParser fhirResourceSectionParser = new FHIRResourceSectionParser(patientSectionParser, ccSectionParser);
        
        Parser parser = new Parser();
        parser.setSectionParsers(patientSectionParser, ccSectionParser, fhirResourceSectionParser);
        parser.parse(csv);
        List<BaseResource> fhirResources = parser.getFHIRResources();
        
        UofAScenarioSerializer serializer = new UofAScenarioSerializer();
        serializer.setAddFormsSteps(true);
        serializer.setAddNotifyPMSStep(true);
        String yaml = serializer.toYaml(fhirResources);
        
        System.out.println("\nYAML:\n"+yaml+"\n\n");
        FileWriter writer = new FileWriter(new File("/tmp/jane-doe.yaml"));
        IOUtils.write(yaml, writer);
        writer.close();
        
        Map<? extends Class<? extends BaseResource>, List<BaseResource>> resourcesByClass = fhirResources.stream().collect(Collectors.groupingBy(r -> r.getClass()));
        
        System.out.println("done");
        
    }
    
}
