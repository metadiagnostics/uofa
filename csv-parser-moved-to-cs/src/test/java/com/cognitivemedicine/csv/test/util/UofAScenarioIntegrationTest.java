/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.test.util;

import ca.uhn.fhir.context.FhirVersionEnum;
import com.cognitivemedicine.cdsp.fhir.client.BaseService;
import com.cognitivemedicine.cdsp.fhir.client.config.FhirConfigurator;
import com.cognitivemedicine.cdsp.simulator.api.ScenarioInstanceEventListenerAdapter;
import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.cognitivemedicine.cdsp.simulator.fhir.config.ContextConfigurator;
import com.cognitivemedicine.cdsp.simulator.instance.ScenarioInstance;
import com.cognitivemedicine.cdsp.simulator.instance.StepInstance;
import com.cognitivemedicine.cdsp.simulator.runtime.FHIRService;
import com.cognitivemedicine.cdsp.simulator.runtime.FhirServiceImpl;
import com.cognitivemedicine.csv.processing.CodeableConceptSectionParser;
import com.cognitivemedicine.csv.processing.FHIRResourceSectionParser;
import com.cognitivemedicine.csv.processing.Parser;
import com.cognitivemedicine.csv.processing.PatientSectionParser;
import com.cognitivemedicine.csv.util.UofAScenarioSerializer;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import org.hl7.fhir.dstu3.model.BaseResource;
import static org.junit.Assert.assertThat;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class UofAScenarioIntegrationTest {
    
    private final String FHIR_ENDPOINT = "http://localhost:8080/hapi-fhir-jpaserver-example/baseDstu3";
    
    /**
     * Multiple Open + Closed resource in a multiple columns.
     * @throws Exception 
     */
    @Test
    @Ignore("Make sure you have a running FHIR server")
    public void runCSV4Test() throws Exception{
        InputStreamReader csv = new InputStreamReader(UofAScenarioIntegrationTest.class.getResourceAsStream("/csv/csv-4.csv"));

        PatientSectionParser patientSectionParser = new PatientSectionParser();
        CodeableConceptSectionParser ccSectionParser = new CodeableConceptSectionParser();
        FHIRResourceSectionParser fhirResourceSectionParser = new FHIRResourceSectionParser(patientSectionParser, ccSectionParser);
        
        Parser parser = new Parser();
        parser.setSectionParsers(patientSectionParser, ccSectionParser, fhirResourceSectionParser);
        parser.parse(csv);
        List<BaseResource> fhirResources = parser.getFHIRResources();
        
        UofAScenarioSerializer serializer = new UofAScenarioSerializer();
        ScenarioDefinition scenarioDefinition = serializer.toScenarioDefinition(fhirResources);
        
        
        ScenarioContext context = new ScenarioContext();
        ContextConfigurator fhirConfigurator = new ContextConfigurator(createFhirService());
        fhirConfigurator.configureContext(context);
        
        
        List<Boolean> errors = new ArrayList<>();
        ScenarioInstance scenarioInstance = scenarioDefinition.createInstance(context);
        scenarioInstance.addEventListener(new ScenarioInstanceEventListenerAdapter(){
            @Override
            public void onScenarioAbort(StepInstance step, Exception e) {
                System.out.println("Exception executing step "+step);
                e.printStackTrace();
                errors.add(Boolean.TRUE);
            }
            
        });
        scenarioInstance.run();
        
        assertThat(errors.isEmpty(), is(true));
        
    }
    
    @Test
    @Ignore("Make sure you have a running FHIR server")
    public void runMBMOnlyEATest() throws Exception{
        InputStreamReader csv = new InputStreamReader(UofAScenarioIntegrationTest.class.getResourceAsStream("/csv/mbm-only-ea.csv"));

        PatientSectionParser patientSectionParser = new PatientSectionParser();
        CodeableConceptSectionParser ccSectionParser = new CodeableConceptSectionParser();
        FHIRResourceSectionParser fhirResourceSectionParser = new FHIRResourceSectionParser(patientSectionParser, ccSectionParser);
        
        Parser parser = new Parser();
        parser.setSectionParsers(patientSectionParser, ccSectionParser, fhirResourceSectionParser);
        parser.parse(csv);
        List<BaseResource> fhirResources = parser.getFHIRResources();
        
        UofAScenarioSerializer serializer = new UofAScenarioSerializer();
        ScenarioDefinition scenarioDefinition = serializer.toScenarioDefinition(fhirResources);
        
        
        ScenarioContext context = new ScenarioContext();
        ContextConfigurator fhirConfigurator = new ContextConfigurator(createFhirService());
        fhirConfigurator.configureContext(context);
        
        
        List<Boolean> errors = new ArrayList<>();
        ScenarioInstance scenarioInstance = scenarioDefinition.createInstance(context);
        scenarioInstance.addEventListener(new ScenarioInstanceEventListenerAdapter(){
            @Override
            public void onScenarioAbort(StepInstance step, Exception e) {
                System.out.println("Exception executing step "+step);
                e.printStackTrace();
                errors.add(Boolean.TRUE);
            }
            
        });
        scenarioInstance.run();
        
        assertThat(errors.isEmpty(), is(true));
        
    }
    
    private FHIRService createFhirService(){
        
        FhirConfigurator configurator = new FhirConfigurator();
        configurator.setRootUrl(FHIR_ENDPOINT);
        configurator.setVersion(FhirVersionEnum.DSTU3);
        
        BaseService baseService = new BaseService(configurator);
        
        return new FhirServiceImpl(baseService);
    }
}
