/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.test.processing;

import com.cognitivemedicine.csv.processing.CodeableConceptSectionParser;
import com.cognitivemedicine.csv.processing.FHIRResourceSectionParser;
import com.cognitivemedicine.csv.processing.PatientSectionParser;
import java.io.BufferedReader;
import java.io.StringReader;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import org.hl7.fhir.dstu3.model.BaseResource;
import org.hl7.fhir.dstu3.model.Condition;
import org.hl7.fhir.dstu3.model.MedicationAdministration;
import org.hl7.fhir.dstu3.model.MedicationRequest;
import org.hl7.fhir.dstu3.model.NutritionOrder;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.dstu3.model.StringType;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

public class FHIRResourceSectionParserTest {

    @Before
    public void doBefore() {
    }

    @Test
    public void valid() {

        PatientSectionParser patientSectionParser = new PatientSectionParser() {

            Patient patient = new Patient();

            @Override
            public Patient getPatient() {
                return patient;
            }

        };

        String ccCSV = ""
            + "RESOURCE DEFINTIONS,\n"
            + "name,code system,code,resource type\n"
            + "Daily Weight (g),http//snomed.info/sct,27113001,Observation,,\n"
            + "Colostrum (cc),http//snomed.info/sct,53875002,Medication Administration,,\n"
            + "Maternal Breast Milk (cc),http//snomed.info/sct,x1111,Medication Administration,,\n"
            + "Donor Breast Milk (cc),http//snomed.info/sct,x2222,Medication Administration,,\n"
            + "PE 20 (cc),http//snomed.info/sct,x3333,Medication Administration,,\n"
            + "PE 22 (cc),http//snomed.info/sct,x4444,Medication Administration,,\n"
            + "PE 24 (cc),http//snomed.info/sct,x5555,Medication Administration,,\n"
            + "D10W  (cc),http//snomed.info/sct,x6666,Medication Administration,,\n"
            + "Feeding Order,http//snomed.info/sct,57794-0,Nutrition Order,,\n"
            + ",,,,,";

        CodeableConceptSectionParser codeableConceptSectionParser = new CodeableConceptSectionParser();
        codeableConceptSectionParser.parse(new BufferedReader(new StringReader(ccCSV)));

        FHIRResourceSectionParser parser = new FHIRResourceSectionParser(patientSectionParser, codeableConceptSectionParser);

        String csv = ""
            + "FHIR RESOURCES,\n"
            + "Date / Time,,Daily Weight (g),Colostrum (cc),"
            + "Maternal Breast Milk (cc),Donor Breast Milk (cc),"
            + "PE 20 (cc),PE 22 (cc),PE 24 (cc),D10W  (cc),Feeding Order\n"
            + "1/1/18 6:54,0.292,100,200,300,400,500,600,700,800,900\n";

        parser.parse(new BufferedReader(new StringReader(csv)));

        List<BaseResource> resources = parser.getResources();

        assertThat(resources, not(nullValue()));
        assertThat(resources.size(), is(16));

        Map<? extends Class<? extends BaseResource>, List<BaseResource>> resourcesByClass = resources.stream().collect(Collectors.groupingBy(r -> r.getClass()));

        assertThat(resourcesByClass.get(Observation.class).size(), is(1));
        assertThat(resourcesByClass.get(MedicationAdministration.class).size(), is(7));
        assertThat(resourcesByClass.get(NutritionOrder.class).size(), is(1));

    }

    @Test
    public void testDynamicCodes() {

        PatientSectionParser patientSectionParser = new PatientSectionParser() {

            Patient patient = new Patient();

            @Override
            public Patient getPatient() {
                return patient;
            }

        };

        String ccCSV = ""
            + "RESOURCE DEFINTIONS,\n"
            + "name,code system,code,resource type,type\n"
            + "DX #1,http//snomed.info/sct,,Condition,dynamic-code,\n"
            + ",,,,,";

        CodeableConceptSectionParser codeableConceptSectionParser = new CodeableConceptSectionParser();
        codeableConceptSectionParser.parse(new BufferedReader(new StringReader(ccCSV)));

        FHIRResourceSectionParser parser = new FHIRResourceSectionParser(patientSectionParser, codeableConceptSectionParser);

        String csv = ""
            + "FHIR RESOURCES,\n"
            + "Date / Time,DX #1,\n"
            + "1/1/18 6:54,367494004/Premature Birth of Newborn\n"
            + "1/1/18 6:54,999999999/Other\n";

        parser.parse(new BufferedReader(new StringReader(csv)));

        List<BaseResource> resources = parser.getResources();

        assertThat(resources, not(nullValue()));
        assertThat(resources.size(), is(2));

        Map<? extends Class<? extends BaseResource>, List<BaseResource>> resourcesByClass = resources.stream().collect(Collectors.groupingBy(r -> r.getClass()));

        assertThat(resourcesByClass.get(Condition.class).size(), is(2));

    }
    
    @Test
    public void testMedReqAdmin() {

        PatientSectionParser patientSectionParser = new PatientSectionParser() {

            Patient patient = new Patient();

            @Override
            public Patient getPatient() {
                return patient;
            }

        };

        String ccCSV = ""
            + "RESOURCE DEFINTIONS,\n"
            + "name,code system,code,resource type,type\n"
            + "Ampicillin,http//snomed.info/sct,31087008,MedicationRequest/MedicationAdministration,medication-request-administration,\n"
            + ",,,,,";

        CodeableConceptSectionParser codeableConceptSectionParser = new CodeableConceptSectionParser();
        codeableConceptSectionParser.parse(new BufferedReader(new StringReader(ccCSV)));

        FHIRResourceSectionParser parser = new FHIRResourceSectionParser(patientSectionParser, codeableConceptSectionParser);

        String csv = ""
            + "FHIR RESOURCES,\n"
            + "Date / Time,Ampicillin,\n"
            + "1/1/18 6:54,Active\n"
            + "1/2/18 6:54,10\n"
            + "1/2/18 6:54,20\n"
            + "1/2/18 6:54,30\n"
            + "1/3/18 6:54,Cancelled\n";

        parser.parse(new BufferedReader(new StringReader(csv)));

        List<BaseResource> resources = parser.getResources();

        assertThat(resources, not(nullValue()));
        assertThat(resources.size(), is(5));

        Map<? extends Class<? extends BaseResource>, List<BaseResource>> resourcesByClass = resources.stream().collect(Collectors.groupingBy(r -> r.getClass()));

        assertThat(resourcesByClass.get(MedicationRequest.class).size(), is(2));
        assertThat(resourcesByClass.get(MedicationAdministration.class).size(), is(3));

    }
    
    @Test
    public void testObservationUnits() {

        PatientSectionParser patientSectionParser = new PatientSectionParser() {

            Patient patient = new Patient();

            @Override
            public Patient getPatient() {
                return patient;
            }

        };

        String ccCSV = ""
            + "RESOURCE DEFINTIONS,\n"
            + "name,code system,code,resource type,type\n"
            + "obs1,http//snomed.info/sct,123,Observation,,\n"
            + "obs2,http//snomed.info/sct,456,Observation,,\n"
            + "obs3,http//snomed.info/sct,789,Observation,,\n"
            + ",,,,,";

        CodeableConceptSectionParser codeableConceptSectionParser = new CodeableConceptSectionParser();
        codeableConceptSectionParser.parse(new BufferedReader(new StringReader(ccCSV)));

        FHIRResourceSectionParser parser = new FHIRResourceSectionParser(patientSectionParser, codeableConceptSectionParser);

        String csv = ""
            + "FHIR RESOURCES,\n"
            + "Date / Time,obs1,obs2,obs3\n"
            + "1/1/18 6:54,100,100/g,100g\n";

        parser.parse(new BufferedReader(new StringReader(csv)));

        List<BaseResource> resources = parser.getResources();

        assertThat(resources, not(nullValue()));
        assertThat(resources.size(), is(3));

        Observation obs1 = (Observation)resources.stream().filter(o -> ((Observation)o).getCode().getCodingFirstRep().getCode().equals("123")).findAny().get();
        Observation obs2 = (Observation)resources.stream().filter(o -> ((Observation)o).getCode().getCodingFirstRep().getCode().equals("456")).findAny().get();
        Observation obs3 = (Observation)resources.stream().filter(o -> ((Observation)o).getCode().getCodingFirstRep().getCode().equals("789")).findAny().get();
        
        assertThat(((Quantity)obs1.getValue()).getUnit(), is(nullValue()));
        assertThat(((Quantity)obs1.getValue()).getCode(), is(nullValue()));
        
        assertThat(((Quantity)obs2.getValue()).getUnit(), is("g"));
        assertThat(((Quantity)obs2.getValue()).getCode(), is("g"));
        
        assertThat(((StringType)obs3.getValue()).primitiveValue(), is("100g"));

    }
    
    @Test
    public void testConditionSeverity() {

        PatientSectionParser patientSectionParser = new PatientSectionParser() {

            Patient patient = new Patient();

            @Override
            public Patient getPatient() {
                return patient;
            }

        };

        String ccCSV = ""
            + "RESOURCE DEFINTIONS,\n"
            + "name,code system,code,resource type,type\n"
            + "c1,http//snomed.info/sct,123,Condition,,\n"
            + ",,,,,";

        CodeableConceptSectionParser codeableConceptSectionParser = new CodeableConceptSectionParser();
        codeableConceptSectionParser.parse(new BufferedReader(new StringReader(ccCSV)));

        FHIRResourceSectionParser parser = new FHIRResourceSectionParser(patientSectionParser, codeableConceptSectionParser);

        String csv = ""
            + "FHIR RESOURCES,\n"
            + "Date / Time,c1\n"
            + "1/1/18 6:54,100\n";

        parser.parse(new BufferedReader(new StringReader(csv)));

        List<BaseResource> resources = parser.getResources();

        assertThat(resources, not(nullValue()));
        assertThat(resources.size(), is(1));

        Condition c1 = (Condition)resources.get(0);
        
        
        assertThat(c1.getSeverity(), not(nullValue()));
        assertThat(c1.getSeverity().getCodingFirstRep().getCode(), is("24484000"));
        assertThat(c1.getSeverity().getCodingFirstRep().getDisplay(), is("Severe"));
        

    }
}
