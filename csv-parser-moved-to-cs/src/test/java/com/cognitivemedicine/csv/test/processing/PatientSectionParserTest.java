/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.test.processing;

import com.cognitivemedicine.csv.processing.PatientSectionParser;
import java.io.BufferedReader;
import java.io.StringReader;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.IsNull.nullValue;

import com.cognitivemedicine.csv.util.FhirUtil;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.Patient;
import static org.junit.Assert.assertThat;

import org.hl7.fhir.dstu3.model.Practitioner;
import org.hl7.fhir.dstu3.model.codesystems.AdministrativeGender;
import org.junit.Before;
import org.junit.Test;

public class PatientSectionParserTest {

    @Before
    public void doBefore() {
    }

    @Test
    public void Valid() throws Exception {
        
        PatientSectionParser parser = new PatientSectionParser();
        
        String csv = ""
            + "PATIENT,\n"
            + "First Name,Last Name,Provider First Name,Provider Last Name,Facility,Gender,MR#,Address,City,State,Zip,Birth Date\n"
            + "Reza,Razavipour,Adam,Jones,Rady's Children's Hospital,female,12345,1111 Main St,San Diego,CA,92121,1/1/18 6:54"
            + ",,,,,";
        
        parser.parse(new BufferedReader(new StringReader(csv)));
        
        Patient patient = parser.getPatient();
        Organization org = parser.getDeptOrganization();
        Organization prov = parser.getProviderOrganization();
        Practitioner practitioner = parser.getPractitioner();
        Date birthDate = FhirUtil.convertCSVTimeStampToDate("1/1/18 6:54");

        assertThat(patient, not(nullValue()));
        assertThat(patient.getNameFirstRep().getGivenAsSingleString(), is("Reza"));
        assertThat(patient.getNameFirstRep().getFamily(), is("Razavipour"));
        assertThat(patient.getGender().toCode(), is(AdministrativeGender.FEMALE.toCode()));
        assertThat(patient.getIdentifierFirstRep().getValue(), is("12345"));
        assertThat(patient.getAddressFirstRep().getLine().get(0).toString(), is("1111 Main St"));
        assertThat(patient.getAddressFirstRep().getCity(), is("San Diego"));
        assertThat(patient.getAddressFirstRep().getState(), is("CA"));
        assertThat(patient.getAddressFirstRep().getPostalCode(), is("92121"));
        assertThat(patient.getBirthDate(), is(birthDate));
        assertThat(org.getName(), is("Rady's Children's Hospital"));
        assertThat(prov.getName(), is("Rady's Children's Hospital"));
        assertThat(practitioner.getNameFirstRep().getFamily(), is("Jones"));
        assertThat(practitioner.getNameFirstRep().getGiven().get(0).toString(), is("Adam"));

    }
}
