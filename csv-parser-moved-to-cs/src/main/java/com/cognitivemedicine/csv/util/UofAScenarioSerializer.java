/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.util;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import com.cognitivemedicine.cdsp.simulator.definition.CreateOrUpdateResourceStepDefinition;
import com.cognitivemedicine.cdsp.simulator.definition.ResourceInstanceDefinition;
import com.cognitivemedicine.cdsp.simulator.definition.ResourceTemplateDefinition;
import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.cognitivemedicine.cdsp.simulator.definition.StepDefinition;
import com.cognitivemedicine.cdsp.simulator.serialization.YamlScenarioSerializer;
import com.cognitivemedicine.cs.simulator.pms.definition.NotifyPMSStepDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.CreateOrUpdateFormStepDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.CreateResponsesStepDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.FormDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.FormInstanceDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.ResponsesDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.ResponsesInstanceDefinition;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.io.IOUtils;
import org.hl7.fhir.dstu3.model.BaseResource;
import org.hl7.fhir.dstu3.model.Patient;

/**
 *
 * @author esteban
 */
public class UofAScenarioSerializer {

    private final FhirContext fhirContext;
    private final IParser fhirParser;
    
    private boolean addFormsSteps;
    private boolean addNotifyPMSStep;
    
    public UofAScenarioSerializer() {
        this.fhirContext = FhirContext.forDstu3();
        this.fhirParser = this.fhirContext.newXmlParser();
    }
    
    
    
    
    public ScenarioDefinition toScenarioDefinition(List<BaseResource> resources){
        List<StepDefinition> steps = this.toStepDefinitions(resources);
        
        ScenarioDefinition scenario = new ScenarioDefinition();
        scenario.setSteps(steps);
        
        return scenario;
    }
    
    public List<StepDefinition> toStepDefinitions(List<BaseResource> resources){
        
        List<StepDefinition> steps = new ArrayList<>();
        
        String patientId = null;
        for (BaseResource resource : resources) {
            steps.add(this.createStepDefinition(resource));
            
            if (resource instanceof Patient){
                patientId = ((Patient)resource).getIdElement().getIdPart();
            }
        }
        
        if (addFormsSteps){
            try{
                steps.add(this.createFormDefinitionStep());
                steps.add(this.createFormResponseStep(patientId));
            } catch (IOException e){
                throw new RuntimeException("Exception creating CreateResponsesStepDefinition or CreateOrUpdateFormStepDefinition", e);
            }
        }
        
        if (addNotifyPMSStep){
            try {
                //TODO: take the latest date from the last step.
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2018-02-12");
                steps.add(this.createNotifyPMSStep(date));
            } catch (ParseException ex) {
                throw new RuntimeException("Exception parsing date", ex);
            }
            
        }
        
        return steps;
    }
    
    public String toYaml(List<BaseResource> resources){
        return this.toYaml(this.toScenarioDefinition(resources));
    }
    
    public String toYaml(ScenarioDefinition scenario){
        YamlScenarioSerializer serializer = new YamlScenarioSerializer();
        return serializer.serializeScenarioDefinition(scenario);
    }

    public void setAddFormsSteps(boolean addFormsSteps) {
        this.addFormsSteps = addFormsSteps;
    }

    public void setAddNotifyPMSStep(boolean addNotifyPMSStep) {
        this.addNotifyPMSStep = addNotifyPMSStep;
    }
    
    private StepDefinition createStepDefinition(BaseResource r){
        
        ResourceTemplateDefinition template = new ResourceTemplateDefinition();
        template.setId(UUID.randomUUID().toString());
        template.setDefinition(fhirParser.encodeResourceToString(r));
        
        ResourceInstanceDefinition instance = new ResourceInstanceDefinition();
        //instance.setId(id); TODO
        instance.setTemplate(template);
        
        CreateOrUpdateResourceStepDefinition stepDefinition = new CreateOrUpdateResourceStepDefinition(instance);
        stepDefinition.setName("Create Resource "+r.getClass().getSimpleName());
        
        return stepDefinition;
        
    }

    private CreateOrUpdateFormStepDefinition createFormDefinitionStep() throws IOException {
        FormDefinition definition = new FormDefinition();
        definition.setId("definition-1");
        definition.setDefinition(IOUtils.toString(new InputStreamReader(
                UofAScenarioSerializer.class.getResourceAsStream("/forms/definition-1.json")
        )));

        FormInstanceDefinition instanceDefinition = new FormInstanceDefinition();
        instanceDefinition.setId("definition-instance-1");
        instanceDefinition.setParameters(new HashMap<>());
        instanceDefinition.setTemplate(definition);

        CreateOrUpdateFormStepDefinition stepDefinition = new CreateOrUpdateFormStepDefinition(instanceDefinition);
        stepDefinition.setName("Create NEC Form");
        
        return stepDefinition;
    }
    
    private CreateResponsesStepDefinition createFormResponseStep(String patientId) throws IOException{
        ResponsesDefinition definition = new ResponsesDefinition();
        definition.setId("responses-1");
        definition.setDefinition(IOUtils.toString(new InputStreamReader(
                    UofAScenarioSerializer.class.getResourceAsStream("/forms/response-1.json")
                )
            )
        );
        
        Map<String, String> params = new HashMap<>();
        params.put("patientId", "value/"+patientId);
        
        ResponsesInstanceDefinition instanceDefinition = new ResponsesInstanceDefinition();
        instanceDefinition.setId("responses-instance-1");
        instanceDefinition.setParameters(params);
        instanceDefinition.setTemplate(definition);
        
        CreateResponsesStepDefinition stepDefinition = new CreateResponsesStepDefinition(instanceDefinition);
        stepDefinition.setName("Create Form Response ("+patientId+")");

        return stepDefinition;
    }
    
    private NotifyPMSStepDefinition createNotifyPMSStep(Date date){
        String topic = "CASE_ALL";
        String data = "value/KTD-NEC-";
        String dataSuffix = "value/"+DateTimeFormatter.ISO_INSTANT.format(date.toInstant());
        
        NotifyPMSStepDefinition step = new NotifyPMSStepDefinition();
        step.setTopic(topic);
        step.setData(data);
        step.setDataSuffix(dataSuffix);
        
        step.setName("Start Reasoning");
        
        return step;
    }
}
