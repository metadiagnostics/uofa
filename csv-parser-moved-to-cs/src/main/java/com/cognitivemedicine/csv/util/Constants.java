/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.util;

public class Constants {
    public static final String FIELD_DELIMITER = ",";
    public static final String COMPOSITE_TYPE_DELIMITER = "/";
    public static final String DYNAMIC_CODE_TYPE_DELIMITER = "/";
    
    public static final String FHIR_OBSERVATION_VALUE_UNIT_DELIMITER = "/";

    // CodeableConcepts
    public static final String CC_DELIMITER_LINE = "RESOURCE DEFINTIONS";

    public static final String CC_NAME = "name";
    public static final String CC_CODE_SYSTEM = "code system";
    public static final String CC_CODE = "code";
    public static final String CC_CATEGORY = "category";
    public static final String CC_RESOURCE_TYPE = "resource type";
    public static final String CC_TYPE = "type";
    public static final String CC_UNITS = "units";
    
    public static final String CC_TYPE_COMPOSITE = "composite";
    public static final String CC_TYPE_DYNAMIC_CODE = "dynamic-code";
    public static final String CC_TYPE_MED_REQ_ADMIN = "medication-request-administration";
    

    // Patient
    public static final String PATIENT_DELIMITER_LINE = "PATIENT";
    public static final String PATIENT_FIRST_NAME = "First Name";
    public static final String PATIENT_LAST_NAME = "Last Name";
    public static final String PATIENT_PROVIDER_FIRST_NAME = "Provider First Name";
    public static final String PATIENT_PROVIDER_LAST_NAME = "Provider Last Name";
    public static final String PATIENT_FACILITY = "Facility";
    public static final String PATIENT_BIRTH_DATE = "Birth Date";
    public static final String PATIENT_GENDER = "Gender";
    public static final String PATIENT_MR_NUMBER = "MR#";
    public static final String PATIENT_ADDRESS_LINE_ONE = "Address";
    public static final String PATIENT_CITY = "City";
    public static final String PATIENT_STATE = "State";
    public static final String PATIENT_ZIP = "Zip";


    // Resource Type
    public static final String FHIR_OBSERVATION = "Observation";
    public static final String FHIR_MEDICATION_ADMINISTRATION = "Medication Administration";
    public static final String FHIR_MEDICATION_REQUEST = "Medication Request";
    public static final String FHIR_STD_NUTRITION_ORDER = "Nutrition Order";
    public static final String FHIR_CONDITION = "Condition";
    public static final String FHIR_PROCEDURE_REQUEST = "Procedure Request";
    public static final String FHIR_DIAGNOSTIC_REPORT = "Diagnostic Report";


    // Columns
    public static final String FHIR_RESOURCE_DELIMITER_LINE = "FHIR RESOURCES";

    public static final String FHIR_RESOURCE_DATE_TIME = "Date / Time";
    public static final String FHIR_RESOURCE_DAILY_WEIGHT = "Daily Weight (g)";
    public static final String FHIR_RESOURCE_COLOSTRUM = "Colostrum (cc)";
    public static final String FHIR_RESOURCE_MATERNAL_BREAST_MILK = "Maternal Breast Milk (cc)";
    public static final String FHIR_RESOURCE_DONOR_BREAST_MILK = "Donor Breast Milk (cc)";


}
