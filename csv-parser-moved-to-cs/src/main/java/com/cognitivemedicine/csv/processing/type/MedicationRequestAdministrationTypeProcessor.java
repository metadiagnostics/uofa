/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.processing.type;

import com.cognitivemedicine.csv.util.Constants;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.dstu3.model.BaseResource;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.MedicationAdministration;
import org.hl7.fhir.dstu3.model.MedicationRequest;
import org.hl7.fhir.exceptions.FHIRException;

/**
 * This class deals with the logic to support
 * {@link Constants#CC_TYPE_MED_REQ_ADMIN} columns.
 * 
 * @author esteban
 */
public class MedicationRequestAdministrationTypeProcessor implements FhirTypeProcessor {
    
    public final static String ORDERED = "Ordered";
    
    private Map<Integer, MedicationRequest> openResourcesByRow = new HashMap<>();

    @Override
    public PreProcessResult preProcess(int colIndex, String resourceType, CodeableConcept codeableConcept, String value) {
        
        boolean isMedRequest = false;
        try {
            //We assume that if the value can be converted to a MedicationRequestStatus
            //a MedicationRequest has to be created.
            MedicationRequest.MedicationRequestStatus.fromCode(value.toLowerCase());
            isMedRequest = true;
        } catch (FHIRException ex) {
        }

        BaseResource openResource = this.openResourcesByRow.get(colIndex);
        if (isMedRequest) {
            //We need to create a MedicationRequest
            resourceType = Constants.FHIR_MEDICATION_REQUEST;
        } else {
            //We need to create a MedicationAdministration
            resourceType = Constants.FHIR_MEDICATION_ADMINISTRATION;
        }

        return new PreProcessResult(codeableConcept, resourceType, value, openResource);
    }

    @Override
    public void postProcess(int colIndex, BaseResource createdResource, PreProcessResult preProcessResult) {
        
        if (createdResource instanceof MedicationRequest){
            //Set the created Resource as the Open Resource of this row.
            this.openResourcesByRow.put(colIndex, (MedicationRequest) createdResource);
        } 
    }
    
}
