/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.processing;

import com.cognitivemedicine.csv.util.Constants;
import com.cognitivemedicine.csv.util.FhirUtil;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class PatientSectionParser extends BaseSectionParser{

    private String[] headers = null;
    private Patient patient;
    private Organization deptOrganization;
    private Organization providerOrganization;
    private Practitioner practitioner;

    @Override
    public String getSectionDelimiter() {
        return Constants.PATIENT_DELIMITER_LINE;
    }
    

    @Override
    protected void parseHeader(String line) {
        this.headers = line.split(Constants.FIELD_DELIMITER);
    }

    @Override
    protected void parseBody(String line) {
        String firstName = "";
        String lastName = "";
        String providerFirstName = "";
        String providerLastName = "";
        String facility = "";
        String mrNumber = "";
        String addressLineOne = "";
        String city = "";
        String state = "";
        String zip = "";
        String gender = "";
        Date birthDate = null;

        String[] parts = line.split(Constants.FIELD_DELIMITER);

        for (int i = 0; i < parts.length; i++) {
            if (Constants.PATIENT_FIRST_NAME.equals(headers[i])) {
                firstName = parts[i];
            } else if (Constants.PATIENT_LAST_NAME.equals(headers[i])) {
                lastName = parts[i];
            } else if (Constants.PATIENT_FACILITY.equals(headers[i])) {
                facility = parts[i];
            } else  if (Constants.PATIENT_PROVIDER_FIRST_NAME.equals(headers[i])) {
                providerFirstName = parts[i];
            } else if (Constants.PATIENT_PROVIDER_LAST_NAME.equals(headers[i])) {
                providerLastName = parts[i];
            } else if (Constants.PATIENT_MR_NUMBER.equals(headers[i])) {
                mrNumber = parts[i];
            } else if (Constants.PATIENT_ADDRESS_LINE_ONE.equals(headers[i])) {
                addressLineOne = parts[i];
            } else if (Constants.PATIENT_CITY.equals(headers[i])) {
                city = parts[i];
            } else if (Constants.PATIENT_STATE.equals(headers[i])) {
                state = parts[i];
            } else if (Constants.PATIENT_ZIP.equals(headers[i])) {
                zip = parts[i];
            } else if (Constants.PATIENT_GENDER.equals(headers[i])) {
                gender = parts[i];
            } else if (Constants.PATIENT_BIRTH_DATE.equals(headers[i])) {
                birthDate = FhirUtil.convertCSVTimeStampToDate(parts[i]);
            }
        }

        patient = new Patient();
        patient.setActive(true);
        patient.setId(UUID.randomUUID().toString());
        patient.setName(FhirUtil.buildName(firstName, lastName));

        try {
            patient.setGender(Enumerations.AdministrativeGender.fromCode(gender));
        } catch (FHIRException e) {
            e.printStackTrace();
            // Log and move on
        }

        patient.setBirthDate(birthDate);

        List<Identifier> idList = new ArrayList<>();
        Identifier mrIdentifier = new Identifier();
        mrIdentifier.setType(FhirUtil.buildCodeableConcept("http://hl7.org/fhir/v2/0203", "MR", ""));
        mrIdentifier.setSystem("urn:oid:1.2.3.4");
        mrIdentifier.setValue(mrNumber);
        idList.add(mrIdentifier);
        patient.setIdentifier(idList);

        List<Address> addressList = new ArrayList<>();
        Address address = new Address();
        address.addLine(addressLineOne);
        address.setCity(city);
        address.setPostalCode(zip);
        address.setState(state);
        addressList.add(address);
        patient.setAddress(addressList);

        deptOrganization = FhirUtil.buildOrganization(UUID.randomUUID().toString(), facility, FhirUtil.buildCodeableConcept("http://hl7.org/fhir/organization-type", "dept", "Department"));
        providerOrganization = FhirUtil.buildOrganization(UUID.randomUUID().toString(), facility, FhirUtil.buildCodeableConcept("http://hl7.org/fhir/organization-type", "prov", "Provider"));
        practitioner = FhirUtil.buildPractitioner(UUID.randomUUID().toString(), providerFirstName, providerLastName);
        PractitionerRole role = FhirUtil.buildPractitionerRole(UUID.randomUUID().toString(), practitioner, deptOrganization);
        
        this.addResource(patient);
        this.addResource(deptOrganization);
        this.addResource(providerOrganization);
        this.addResource(practitioner);
        this.addResource(role);
    }
    
    public Patient getPatient() {
        return this.patient;
    }

    public Organization getDeptOrganization() {
        return this.deptOrganization;
    }

    public Organization getProviderOrganization() {
        return providerOrganization;
    }

    public Practitioner getPractitioner() {
        return this.practitioner;
    }
}
