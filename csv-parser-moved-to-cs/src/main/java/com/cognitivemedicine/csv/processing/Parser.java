/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.processing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.hl7.fhir.dstu3.model.BaseResource;

/**
 *
 * @author esteban
 */
public class Parser {
    
    private final List<SectionParser> sectionParsers = new ArrayList<>();
    
    //We keep a reference to the FHIRResourceSectionParser to make it easier
    protected FHIRResourceSectionParser fhirResourceSectionParser;

    public void setSectionParsers(SectionParser... parsers){
        this.sectionParsers.addAll(Arrays.asList(parsers));
        
        
        for (SectionParser parser : parsers) {
            if (parser instanceof FHIRResourceSectionParser){
                if (this.fhirResourceSectionParser != null){
                    throw new IllegalArgumentException("Only 1 FHIRResourceSectionParser is supported");
                }
                this.fhirResourceSectionParser = (FHIRResourceSectionParser) parser;
            }
        }
        
    }
    
    public void parse(String inFilename) throws Exception{
        this.parse(new FileReader(inFilename));
    }
    
    public void parse(Reader reader) throws Exception{
        
        if (this.fhirResourceSectionParser == null){
            throw new IllegalArgumentException("No FHIRResourceSectionParser was configured.");
        }
        
        BufferedReader br = new BufferedReader(reader);
        
        for (SectionParser sectionParser : sectionParsers) {
            sectionParser.parse(br);
        }
        
    }
    
    public List<BaseResource> getFHIRResources(){
        List<BaseResource> resources = new ArrayList<>();
        for (SectionParser sectionParser : sectionParsers) {
            resources.addAll(sectionParser.getResources());
        }
        return resources;
    }
    
    
    
    
}
