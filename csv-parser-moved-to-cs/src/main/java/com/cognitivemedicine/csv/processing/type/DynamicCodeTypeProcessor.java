/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.processing.type;

import com.cognitivemedicine.csv.util.Constants;
import com.cognitivemedicine.csv.util.FhirUtil;
import org.hl7.fhir.dstu3.model.BaseResource;
import org.hl7.fhir.dstu3.model.CodeableConcept;

/**
 * This class deals with the logic to support
 * {@link Constants#CC_TYPE_DYNAMIC_CODE} columns.
 * 
 * @author esteban
 */
public class DynamicCodeTypeProcessor implements FhirTypeProcessor {

    @Override
    public PreProcessResult preProcess(int colIndex, String resourceType, CodeableConcept codeableConcept, String value) {
        //parse the actual value to get the Code and Display
        String[] values = value.split(Constants.DYNAMIC_CODE_TYPE_DELIMITER);

        if (values.length != 2) {
            throw new IllegalStateException("A Dynamic-code Type must specify 2 elemnts. Found: " + values);
        }

        codeableConcept = FhirUtil.buildCodeableConcept(codeableConcept.getCodingFirstRep().getSystem(), values[0].trim(), values[1].trim());
        
        return new PreProcessResult(codeableConcept, resourceType, value, null);
    }

    @Override
    public void postProcess(int colIndex, BaseResource createdResource, PreProcessResult preProcessResult) {
        //Do nothing
    }

}
