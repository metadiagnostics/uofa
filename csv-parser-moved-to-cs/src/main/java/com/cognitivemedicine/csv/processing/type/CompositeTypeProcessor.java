/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.processing.type;

import com.cognitivemedicine.csv.util.Constants;
import java.util.HashMap;
import java.util.Map;
import org.hl7.fhir.dstu3.model.BaseResource;
import org.hl7.fhir.dstu3.model.CodeableConcept;

/**
 * This class deals with the logic to support
 * {@link Constants#CC_TYPE_COMPOSITE} columns.
 *
 * @author esteban
 */
public class CompositeTypeProcessor implements FhirTypeProcessor {

    private Map<Integer, BaseResource> openResourcesByRow = new HashMap<>();

    @Override
    public PreProcessResult preProcess(int colIndex, String resourceType, CodeableConcept codeableConcept, String value) {

        String[] resourceTypeParts = resourceType.split(Constants.COMPOSITE_TYPE_DELIMITER);

        if (resourceTypeParts.length != 2) {
            throw new IllegalStateException("A Composite Type must specify 2 FHIR Resources. Found: " + resourceType);
        }

        BaseResource openResource = this.openResourcesByRow.get(colIndex);
        if (openResource != null) {
            //There is already an Open resource for this column. 
            //We need to use the second part of the resource type.
            resourceType = resourceTypeParts[1];
        } else {
            //There is no Open resource for this column. 
            //We need to use the first part of the resource type.
            resourceType = resourceTypeParts[0];
        }

        return new PreProcessResult(codeableConcept, resourceType, value, openResource);
    }

    @Override
    public void postProcess(int colIndex, BaseResource createdResource, PreProcessResult preProcessResult) {
        if (preProcessResult.getOpenResource() != null) {
            //The Resource is now Closed. Remove it from the map.
            this.openResourcesByRow.remove(colIndex);
        } else {
            //Set the created Resource as the Open Resource of this row.
            this.openResourcesByRow.put(colIndex, createdResource);
        }
    }

}
