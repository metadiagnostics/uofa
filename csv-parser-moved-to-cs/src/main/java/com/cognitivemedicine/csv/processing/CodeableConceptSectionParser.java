/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.processing;

import com.cognitivemedicine.csv.util.Constants;
import com.cognitivemedicine.csv.util.FhirUtil;
import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.dstu3.model.CodeableConcept;

import java.util.HashMap;
import java.util.Map;

public class CodeableConceptSectionParser extends BaseSectionParser {

    public static class CodeableConceptEntry {

        private final String resourceType;
        private final String type;
        private final String category;
        private final String units;
        private final CodeableConcept concept;

        public CodeableConceptEntry(CodeableConcept concept, String resourceType, String type, String category, String units) {
            this.concept = concept;
            this.resourceType = resourceType;
            this.type = type;
            this.category = category;
            this.units = units;
        }

        public String getResourceType() {
            return resourceType;
        }

        public String getType() {
            return this.type;
        }

        public CodeableConcept getCodeableConcept() {
            return concept;
        }

        public boolean isComposite() {
            return Constants.CC_TYPE_COMPOSITE.equals(type);
        }

        public boolean isDynamicCode() {
            return Constants.CC_TYPE_DYNAMIC_CODE.equals(type);
        }

        public String getCategory() {
            return category;
        }

        public String getUnits() {
            return this.units;
        }
    }

    private final Map<String, CodeableConceptEntry> codeableConceptByDisplay = new HashMap<>();
    private String[] headers = null;

    public CodeableConceptSectionParser() {
    }

    @Override
    public String getSectionDelimiter() {
        return Constants.CC_DELIMITER_LINE;
    }

    @Override
    protected void parseHeader(String line) {
        this.headers = line.split(Constants.FIELD_DELIMITER);
    }

    @Override
    protected void parseBody(String line) {
        String[] parts = line.split(Constants.FIELD_DELIMITER);
        String display = null;
        String units = null;
        String code = null;
        String resourceType = null;
        String type = null;
        String codeSystem = null;
        String category = null;

        for (int i = 0; i < headers.length; i++) {
            if (Constants.CC_NAME.equals(headers[i])) {
                display = parts[i];
            } else if (Constants.CC_CODE_SYSTEM.equals(headers[i])) {
                codeSystem = parts[i];
            } else if (Constants.CC_CODE.equals(headers[i])) {
                code = parts[i];
            } else if (Constants.CC_RESOURCE_TYPE.equals(headers[i])) {
                resourceType = parts[i];
            } else if (Constants.CC_TYPE.equals(headers[i])) {
                type = parts.length > i ? parts[i] : null;
            } else if (Constants.CC_CATEGORY.equals(headers[i]) && parts.length > i) {
                category = parts[i];
            } else if (Constants.CC_UNITS.equals(headers[i])  && parts.length > i) {
                units = parts[i];
            }
        }

        if (display == null || (!Constants.CC_TYPE_DYNAMIC_CODE.equals(type) && code == null) || resourceType == null || codeSystem == null) {
            throw new IllegalArgumentException("malformed codeable concept defintions: " + line);
        }

        String codeDisplay = display.trim();
        if (!StringUtils.isBlank(units)) {
            codeDisplay = codeDisplay + " (" + units + ")";
        }

        CodeableConcept codeableConcept = FhirUtil.buildCodeableConcept(codeSystem.trim(), code != null ? code.trim() : code, codeDisplay);
        CodeableConceptEntry codeableConceptEntry = new CodeableConceptEntry(codeableConcept, resourceType, type, category, units);
        this.codeableConceptByDisplay.put(display.trim(), codeableConceptEntry);

    }

    public CodeableConcept getCodeableConcept(String display) {
        CodeableConceptEntry cce = this.codeableConceptByDisplay.get(display);
        return cce == null ? null : cce.getCodeableConcept();
    }

    public CodeableConceptEntry getCodeableConceptEntry(String display) {
        CodeableConceptEntry cce = this.codeableConceptByDisplay.get(display);

        if (cce == null) {
            throw new IllegalArgumentException("Unknown CodeableConceptEntry for CodeableConcept " + display);
        }

        return cce;
    }
}
