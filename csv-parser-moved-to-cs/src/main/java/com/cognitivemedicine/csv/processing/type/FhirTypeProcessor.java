/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.processing.type;

import com.cognitivemedicine.csv.util.Constants;
import org.hl7.fhir.dstu3.model.BaseResource;
import org.hl7.fhir.dstu3.model.CodeableConcept;

/**
 * Implementations of this interface know how to deal with the different
 * types of FHIR columns. I.e. {@link Constants#CC_TYPE_COMPOSITE} or
 * {@link Constants#CC_TYPE_DYNAMIC_CODE}.
 * 
 * @author esteban
 */
public interface FhirTypeProcessor {
    
    public static class PreProcessResult{
        private final CodeableConcept codeableConcept;
        private final String resourceType;
        private final String value;
        private final BaseResource openResource;

        public PreProcessResult(CodeableConcept codeableConcept, String resourceType, String value, BaseResource openResource) {
            this.codeableConcept = codeableConcept;
            this.resourceType = resourceType;
            this.value = value;
            this.openResource = openResource;
        }

        public CodeableConcept getCodeableConcept() {
            return codeableConcept;
        }

        public String getResourceType() {
            return resourceType;
        }
        
        public String getValue() {
            return value;
        }

        public BaseResource getOpenResource() {
            return openResource;
        }
        
    }
    
    public PreProcessResult preProcess(int colIndex, String resourceType, CodeableConcept codeableConcept, String value);
    public void postProcess(int colIndex, BaseResource createdResource, PreProcessResult preProcessResult);
    
}
