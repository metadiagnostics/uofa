/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.ufoa.jms;

import com.cognitivemedicine.cdsp.cwf.jms.JMSServiceFactory;

/**
 *
 * @author esteban
 */
public class Main {
    
    private PublisherHelper publisher;
    
    public static void main(String[] args) {
        
        String data = "KTD-NEC-2018-02-11T23:00:00Z";
        
        if (args.length > 0){
            data = args[0];
        }
        
        String topic = "CASE_ALL";
        new Main("JMS_CHANNEL_RESPONSE").sendMessage(topic, data);
        System.out.println("Sent");
    }
    
    
    public Main(String topic){
        String url = "localhost:7676";
        String discriminator = JMSServiceFactory.ContextKeys.JMS_CASE_EVENT_DISCRIMINATOR_VALUE;
        this.publisher = new PublisherHelper(url, topic, "DSSRequestId");
        
    }
    
    public void sendMessage(String topic, String data){
        publisher.publishString(topic, data);
    }
}
