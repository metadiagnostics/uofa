/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.ufoa.jms;

/**
 * *****************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc
 * (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************
 */
import java.util.ArrayList;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.cognitivemedicine.cdsp.bom.fhir.CORACognitiveBase;
import com.cognitivemedicine.cdsp.bom.fhir.CORAPatientAdapter;
import com.cognitivemedicine.cdsp.cwf.jms.JMSServiceFactory;
import com.cognitivemedicine.cdsp.messaging.jms.JMSMessagePublisher;
import com.sun.messaging.ConnectionConfiguration;

/**
 *
 * @author esteban
 */
public class PublisherHelper {

    private static final Log LOG = LogFactory.getLog(PublisherHelper.class);
    private final String discriminatorProperty;
    private final JMSMessagePublisher jmsProducer;

    public PublisherHelper() {
        // JMS Stuff
        JMSServiceFactory jmsHelper = new JMSServiceFactory();
        this.discriminatorProperty
            = jmsHelper.getConfigUtils().getString(JMSServiceFactory.ContextKeys.JMS_CASE_EVENT_DISCRIMINATOR_VALUE);
        this.jmsProducer = jmsHelper.getCaseStateMessagePublisher();
    }

    public PublisherHelper(String jmsConnect, String topic, String discriminatorProperty) {

        this.discriminatorProperty = discriminatorProperty;
        this.jmsProducer
            = new JMSMessagePublisher(createConnectionFactory(jmsConnect), topic, discriminatorProperty);
    }

    public void publishString(String topic, String data) {
        this.jmsProducer.publish(data, topic);
    }

    public void publishCognitiveBaseResource(CORACognitiveBase resource, String discriminatorValue) {
        this.jmsProducer.publish(resource, discriminatorValue);
    }

    public void publishPatient(String topic, CORAPatientAdapter patient) {
        publishCognitiveBaseResource(patient, topic);
    }

    public void publishList(ArrayList<Object> list, String topic) {
        this.jmsProducer.publish(list, topic);
    }

    private ConnectionFactory createConnectionFactory(String imqAddressList) {
        try {
            com.sun.messaging.ConnectionFactory connectionFactory = new com.sun.messaging.ConnectionFactory();
            connectionFactory.setProperty(ConnectionConfiguration.imqAddressList, imqAddressList);
            connectionFactory.setProperty(ConnectionConfiguration.imqReconnectEnabled, "true");
            return connectionFactory;
        } catch (JMSException e) {
            throw new IllegalStateException("There was an error configure JMS channel, it will not be configured", e);
        }
    }
}
