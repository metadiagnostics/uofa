/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.ufoa.wps;

/**
 * *****************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc
 * (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * *****************************************************************************
 */
import com.cognitivemedicine.cdps.ktd.commons.CommunicationRequestHelper;
import com.cognitivemedicine.cdps.ktd.commons.KTDInformation;
import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicalmodel.Action;
import com.cognitivemedicine.fhir.logicalmodel.CommunicationRequest;
import com.cognitivemedicine.fhir.logicalmodel.Context;
import com.cognitivemedicine.fhir.logicalmodel.Recipient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.Ignore;

/**
 *
 * @author esteban
 */
public class WPSTest {

    private final String wpsURL = "http://localhost:9090/cdspwps/services/api/v1";
    //private final String wpsURL = "http://localhost:8080/cdspws/services/api/v1";
    
    @Test
    @Ignore("It requires a running JMS Server")
    public void persistCR() throws Exception {
        String title = "University of Arizona Sample";
        String body = "This is just a sample";
        String subjectId = "Patient/1";
        String caseId = "2";
        
        //KTDInfo
        KTDInformation ktdInformation = new KTDInformation("UofA", "test", "sample");

        //Context
        IdentifierDt subjectIdentifier = new IdentifierDt();
        subjectIdentifier.setValue(subjectId);
        IdentifierDt caseIdentifier = new IdentifierDt();
        caseIdentifier.setValue(caseId);
        Context ctx = new Context();
        ctx.setSubjectId(subjectIdentifier);
        ctx.setCaseId(caseIdentifier);

        //Payload Data
        CommunicationRequest.Payload payload = CommunicationRequestHelper.createPayload(title, body);

        //Misc Data
        String topicIdentifier = "UofA";
        String status = org.hl7.fhir.dstu3.model.CommunicationRequest.CommunicationRequestStatus.ACTIVE.name();

        //Category Data
        CodingDt category = CommunicationRequestHelper.getCodingDt(
            "com.cognitivemedicine.codesystem.communicationsCategory",
            "123",
            "Advice"
        );

        //Recipients
        Recipient recipient1 = CommunicationRequestHelper.createRecipient(
            "com.cognitivemedicine.codesystem.RecipientType",
            "ROLE",
            "Role",
            "com.cognitivemedicine.codesystem.Role",
            "Attending"
        );

        List<Recipient> recipients = java.util.Arrays.asList(recipient1);

        //Priority Data
        CodingDt priority = CommunicationRequestHelper.getCodingDt(
            "com.cognitivemedicine.codesystem.Priority",
            "MEDIUM",
            "Medium"
        );

        // Provenance
        String provenance = "https://www.aagbi.org/sites/default/files/MH%20guideline%20for%20web%20v2.pdf";

        //Actions
        List<Action> actions = CommunicationRequestHelper.getDefaultActions();

        CommunicationRequest cr = CommunicationRequestHelper.createCommunicationRequest(
            ctx,
            topicIdentifier,
            status,
            provenance,
            category,
            priority,
            payload,
            recipients,
            actions,
            "PTM",
            ktdInformation
        );

        cr.setRequestedOn(new java.util.Date());

        sendAlert(cr);
        
    }
    
    private void sendAlert(CommunicationRequest req) throws JsonProcessingException, UnsupportedEncodingException, IOException{
        HttpClient client = HttpClientBuilder.create().build();
		ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(req);
        StringEntity entity = new StringEntity((String) content);
        entity.setContentType(ContentType.APPLICATION_JSON.getMimeType());
        HttpUriRequest post = RequestBuilder.post().setUri(wpsURL).setEntity(entity).build();
        HttpResponse response = client.execute(post);
        
        assertThat(response.getStatusLine().getStatusCode(), is(200));
        
    }

}
